﻿

To build Angular App
			ng build --deploy-url /dist/

To build Angular App in production
			ng build  --deploy-url /dist/ --prod --env=prod --output-hashing -aot -vc=true

To Run dotnet
			dotnet run 
