import { Component } from '@angular/core';

@Component({
  template: `
  <div style="padding:3vw;">
    <img src="/assets/img/logo/bitrrexchange2.png" class="img-responsive center-block" id="logo-image">
</div>
<h2 style="text-align:center">Page not found</h2>`
})
export class PageNotFoundComponent {}