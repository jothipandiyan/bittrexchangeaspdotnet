import { Component, Input } from '@angular/core';
import { Market } from 'app/models';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'table-layout',
    templateUrl: 'table-layout.component.html'
})
export class TableLayoutComponent implements OnChanges {
    productList
    @Input() records: any[];
    @Input() settings
    keys: string[];
    @Input() title
    @Input() headers
    filteredItems: Market[];
    pages: number = 4;
    pageSize: number = 10;
    pageNumber: number = 0;
    currentIndex: number = 1;
    items: Market[];
    pagesIndex: Array<number>;
    pageStart: number = 1;
    inputName: string = '';
    isDesc: boolean = false;
    column: string = '';
    direction: number;
    constructor() {

    };
    init() {
        this.currentIndex = 1;
        this.pageStart = 1;
        this.pages = 4;

        this.pageNumber = parseInt("" + (this.filteredItems.length / this.pageSize));
        if (this.filteredItems.length % this.pageSize != 0) {
            this.pageNumber++;
        }

        if (this.pageNumber < this.pages) {
            this.pages = this.pageNumber;
        }

        this.refreshItems();
    }

    ngOnChanges() {
        if (this.records && this.records.length > 0) {
            this.productList = this.records
            this.keys = this.settings
            this.filteredItems = this.productList;
            this.init();
        } else {
            this.keys = this.settings
        }
    }
    FilterByName() {
        this.filteredItems = [];
        if (this.inputName != "" && this.productList) {
            this.productList.forEach(element => {
                var keys = Object.keys(element)
                this.keys.forEach((key: any) => {
                    var value = element[key.fieldName]
                    var valueType = typeof value
                    if (valueType === 'string') {
                        if (value.toUpperCase().indexOf(this.inputName.toUpperCase()) >= 0) {
                            if (this.filteredItems.indexOf(element) < 0)
                                this.filteredItems.push(element);
                        }
                    } else if (valueType === 'number') {
                        if (value == this.inputName) {
                            if (this.filteredItems.indexOf(element) < 0)
                                this.filteredItems.push(element);
                        }

                    }
                });
            });
        } else {
            this.filteredItems = this.productList;
        }
        if(this.filteredItems)
            this.init();
    }
    fillArray(): any {
        var obj = new Array();
        for (var index = this.pageStart; index < this.pageStart + this.pages; index++) {
            obj.push(index);
        }
        return obj;
    }
    refreshItems() {
        this.items = this.filteredItems.slice((this.currentIndex - 1) * this.pageSize, (this.currentIndex) * this.pageSize);
        this.pagesIndex = this.fillArray();
    }
    prevPage() {
        if (this.currentIndex > 1) {
            this.currentIndex--;
        }
        if (this.currentIndex < this.pageStart) {
            this.pageStart = this.currentIndex;
        }
        this.refreshItems();
    }
    nextPage() {
        if (this.currentIndex < this.pageNumber) {
            this.currentIndex++;
        }
        if (this.currentIndex >= (this.pageStart + this.pages)) {
            this.pageStart = this.currentIndex - this.pages + 1;
        }

        this.refreshItems();
    }
    setPage(index: number) {
        this.currentIndex = index;
        this.refreshItems();
    }
    isNumber(val) { return typeof val === 'number'; }
    sort(property) {
        this.isDesc = !this.isDesc; //change the direction    
        this.column = property;
        this.direction = this.isDesc ? 1 : -1;
    };
}