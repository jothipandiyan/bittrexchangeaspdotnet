export const navigation = [
  {
    name: 'Currencies',
    url: '/admin/currencies',
    icon: 'fa fa-btc',
  },
  {
    name: 'Accounts',
    url: '/admin/users',
    icon: 'fa fa-users',
  }

];
