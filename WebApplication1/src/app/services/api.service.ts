import { Injectable } from '@angular/core';
import { Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Http } from '@angular/http';

@Injectable()
export class ApiService {
    apiRoot = "api/Markets"
    apiMarketRoot = "api/Market"

    constructor(private http: HttpClient) {
    }

    getMarket(market) {
        let url = `${this.apiRoot}/GetMarket/?Marketshortcode=${market}`;
        // get users from api
        return this.http.get(url)
    }
    getMarketSummary(currencyShortName) {
        let url = `${this.apiRoot}/GetMarketSummary?currencyshortname=${currencyShortName}`;
        // get users from api
        return this.http.get(url)
    }
    getMarketBidData(currencyShortName) {
        let url = `${this.apiRoot}/GetBidsAgCur?currencyshortname=${currencyShortName}`;
        // get users from api
        return this.http.get(url)
    }
    getMarketAskData(currencyShortName) {
        let url = `${this.apiRoot}/GetAskAgCur?currencyshortname=${currencyShortName}`;
        // get users from api
        return this.http.get(url)
    }
    getHistoryData(currencyShortName) {
        let url = `${this.apiRoot}/GetHistoryAgCur?currencyshortname=${currencyShortName}`;
        // get users from api
        return this.http.get(url)
    }
    getDashboardHeader() {
//        let url = `${this.apiRoot}/getmarketdash`;
        let url = `${this.apiRoot}/MarketDashHeader`;
        // get users from api
        return this.http.get(url)
    }





}