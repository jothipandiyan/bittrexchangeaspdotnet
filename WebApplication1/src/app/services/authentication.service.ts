import { Injectable } from '@angular/core';
import { Http, Headers, Response,RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UserService } from 'app/services';
import { HttpHeaders } from '@angular/common/http/src/headers';

@Injectable()
export class AuthenticationService {
    public token: string;
    isLogin = false;
    isValidUser = false;
    tokenKey = "currentUser"
    hostName = "104.236.17.75/"
    loginApiRoot = "/api/Account"
    userRole 
    isUser
    isAdmin
    userEmail

    constructor(private http: Http,  private cookieService: CookieService, private router: Router) {
        // set token if saved in cookies
        if (this.isAuthencticated()) {
            this.token = this.getToken();
            this.userEmail = this.get("userEmail");
            this.isLogin = true;
        }
    }
    registerUser(user) {

        return this.http.post('api/Account/register', user)
    }
    login(username: string, password: string): Observable<any> {
        
       var body =  {
            "Email":username,
            "Password":password
            }
          
        return this.http.post(`${this.loginApiRoot}/Login`, body)
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let token = response.text() ;

                if (token && token!="INVALID_LOGIN_ATTEMPT" ) {
                    if (token =="Oops! seems you have not confimed your email"){
                        return {result:false,message:token};
                    }

                    // set token property
                    this.token = token;
                    this.isLogin = true;
                    this.isValidUser = true

                    // store username and jwt token in cookies to keep user logged in between page refreshes
                    this.setToken(username, token)
                    this.userEmail = username
                    this.router.navigate(['/markets']);
                    

                    // return true to indicate successful login
                    return {result:true,message:token};
                } else {
                    // return false to indicate failed login
                    return {result:false,message:token};
                }
            });
    }
    getUserRole(){
        let headers= new Headers();
        headers.append("Authorization", `Bearer ${this.getToken()}`)
        let options = new RequestOptions({ headers: headers});

        return this.http.get('api/account/GetUserRoles',options).map(res=> res.json())
       .subscribe(data=>{
        this.userRole = data[0]
        var isAdminRole = this.userRole !='superadmin'
        this.isUser = this.isLogin && isAdminRole 
        this.isAdmin = this.isLogin && !isAdminRole
       });
   }
    logout(): void {
        // clear token remove user from cookies storage to log user out
        this.token = null;
        this.deleteToken();
        this.isLogin = false;
        this.isAdmin = false
        this.isUser = false
        
        this.router.navigate(['/markets']);

    }

    isAuthencticated() {

        return this.cookieService.check(this.tokenKey);
    }
    getToken() {
        return this.cookieService.get(this.tokenKey);

    }
    get(key) {
        return this.cookieService.get(key);

    }
    setToken(username, userToken) {
        this.cookieService.set("userEmail",  username );        
        this.cookieService.set(this.tokenKey,  userToken );
    }
    deleteToken() {

        this.cookieService.delete(this.tokenKey);
        console.log(this.cookieService.get(this.tokenKey))
    }
}