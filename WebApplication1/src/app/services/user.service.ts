import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from './index';
import { User } from '../models/index';
import { HttpHeaderResponse } from '@angular/common/http/src/response';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable()
export class UserService {

    isLogin;
    walletApi = 'api/wallet'
    marketsApiRoot = 'api/Markets'
    constructor(
        private http: HttpClient,
        private authenticationService: AuthenticationService) {
    }

    
    getUsers(): Observable<User> {
        // get users from api
        return this.http.get('api/Users/GetUsers')
            .map((data:User) => data);
    }
 
    getWalletBalanceByCurrency(currency){
        return this.http.get(`${this.walletApi}/GetWalletBalCur?CurShort=${currency}`)
        .map((data:number) => data);
    }
    getLastBidPriceByMarket(currency) {
        return this.http.get(`${this.marketsApiRoot}/GetLastBidPrice?currencyshortname=${currency}`)
            .map((data: number) => data);

    }
    getLastAskPriceByMarket(currency) {
        return this.http.get(`${this.marketsApiRoot}/GetLastAskPrice?currencyshortname=${currency}`)
            .map((data: number) => data);

    }
    getLastPriceByMarket(currency) {
        return this.http.get(`${this.marketsApiRoot}/GetLastPrice?currencyshortname=${currency}`)
                    .map((data: number) => data);
    }
    postBid(data) {
        let url = `${this.marketsApiRoot}/BuyCurUser`;
        // get users from api

        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json')
        return this.http.post(url, data, { headers: headers, responseType: 'text' })
    }
    postAsk(data) {
        let url = `${this.marketsApiRoot}/sellCurUser`;
        // get users from api

        let headers = new HttpHeaders();
        headers = headers.set('Content-Type', 'application/json')
        return this.http.post(url, data, { headers: headers, responseType: 'text' })
    }

    getOpenOrders(){         
        let url = `api/orders/GetOpenOrders`;
        return this.http.get(url)
    }
    
    getClosedOrders(){         
        let url = `api/orders/GetCompletedOrders`;
        return this.http.get(url)
    } 
    getTotalBalance(){         
        let url = `api/useraccount/gettotalbalance`;
        return this.http.get(url)
    }
         
    getOpenOrdersByMarket(market) {
        let url = `api/market/GetOpenOrdersMarket?currencyshortname=${market}`;
        return this.http.get(url)
    }
}