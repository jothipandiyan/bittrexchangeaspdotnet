import { Component } from '@angular/core';
import { AuthenticationService, AuthGuard, UserService } from '../../services/index';
import { Router } from '@angular/router';
declare let $;

export interface FormModel {
    captcha?: string;
  }

  
@Component({
    selector: 'app-header',
    templateUrl: './app-header.component.html',
    styles: [`
      .error { color: crimson; }
      .success { color: green; }
  ` ]
})
export class AppHeaderComponent {
    public formModel: FormModel = {};

    isLogin;
    public myModal;
    model: any = {};
    error = '';
    rememberMe
    isUser = false
    isAdmin = false
    constructor( private router: Router, private userService: UserService, public authenticationService: AuthenticationService) 
        { }

    ngOnInit() {
        this.isLogin = this.authenticationService.isLogin;
 
        if(this.isLogin)
         this.authenticationService.getUserRole()

    }


    logout() {

        this.authenticationService.logout();
        this.authenticationService.isLogin = false;
        this.isLogin = false;
    }
}
