import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common"

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BaseRequestOptions } from '@angular/http';
import { UserService, AuthenticationService, AuthGuard } from '../../services/index'

import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { CookieService } from 'ngx-cookie-service';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    LoginRoutingModule,
    FormsModule,    
    SharedModule
  ],
  declarations: [ LoginComponent,
   ],
  providers: [BaseRequestOptions ],
})
export class LoginModule { }
