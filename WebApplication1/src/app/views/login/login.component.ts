import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, UserService } from '../../services/index';
export interface FormModel {
    captcha?: string;
  }

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  
    public formModel: FormModel = {};

    isLogin;
    public myModal;
    model: any = {};
    error = '';
    rememberMe
    isUser = false
    isAdmin = false
    constructor(
        private router: Router, private userService: UserService,
        private authenticationService: AuthenticationService) { }
  
    ngOnInit() {
    }
  
    login(myModal) {
        this.error = null;
        this.authenticationService.login(this.model.username, this.model.password)
        .subscribe(data => {
            var result = data.result
            if (result === true) {
                this.isLogin = true;
                this.router.navigate(['/markets']);
                //get user role
                this.authenticationService.getUserRole()
                this.formModel.captcha = ''
            } else {
                this.error = data.message;
                this.isLogin = false;
                this.formModel.captcha = ''

            }
        });
    }
}
 