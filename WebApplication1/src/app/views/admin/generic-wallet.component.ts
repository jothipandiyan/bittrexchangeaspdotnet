import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'app/services';

@Component({
  selector: 'generic-wallet',
  templateUrl: './generic-wallet.component.html'
})
export class GenericWallet implements OnInit {
 
  constructor( private router: Router, private admin:AdminService ) { }
  user:any = { };

  ngOnInit(): void {
    this.admin.getWalletList().subscribe(data => console.log(data))
    
  }
  createGenericWallet(){
    let data = {
      "AccountID": 10,
      "CurrencyId": 4,
      "WalletAddress": "749easdadad",
      "PrivateKey": "AasdadS",
      "PublicKey": "ADAasdasdSDA",
      "Mobile": "801534090",
      "Label": "asdasd",
      "Email": "jp@gmail.com",
      "Guid": "zdasdsad"
      }
  }
}
