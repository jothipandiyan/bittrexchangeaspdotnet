import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { BaseRequestOptions } from '@angular/http';

import { AuthenticationService, UserService, AuthGuard, AdminService } from 'app/services/index';

import { AdminManageComponent, UserListComponent, CurrencyComponent } from './index';
import { AdminWalletComponent, ResetPasswordComponent, BidAskComponent, TransHistoryComponent } from './manage/index';

import { AdminRoutingModule } from './admin-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { GenericWallet } from 'app/views/admin/generic-wallet.component';
import { TokenInterceptor } from 'app/interceptor/tokenInterceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { MomentModule } from 'angular2-moment';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    AdminRoutingModule,SharedModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpModule,
    ModalModule,MomentModule
  ],
  declarations: [ AdminManageComponent, CurrencyComponent,GenericWallet, UserListComponent, AdminWalletComponent, ResetPasswordComponent, BidAskComponent, TransHistoryComponent ],
  providers: [BaseRequestOptions, AdminService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true }
  ],

})
export class AdminModule { }
