import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl, EmailValidator } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService, AuthenticationService, AdminService } from 'app/services';

@Component({
  selector: 'user-list',
  templateUrl: './user-list.component.html'
})
export class UserListComponent implements OnInit {


  signUpForm: FormGroup;
  keys
  user: any = { "acname": "", "emailid": "", "mobile": "", "appuser": { "email": "", "Password": "" } };
  error;
  users
  errorResponse
  errorMessage
  isDesc: boolean = false;
  column: string = '';
  direction: number;
  userHeader
  constructor(private router: Router, private userService: UserService, private authService:AdminService) { }

  dataJSON = [{ "MarketName": "BTC-ZEN", "Currency": "Zencash", "Volume": "217.043", "Changes": "-11.0", "LastPrice": "0.00279140", "High": "0.00318628", "Low": "0.00269720", "Spread": "0.6%", "Added": "06/05/2017", "img": "bitcoin.png", "Status": "Biggest % Gain" }];

  headerSettings = [{
    "visible": false,
    "fieldName": "accountid",
    "displayName": "AccountId"
  }, {
    "visible": true,
    "fieldName": "acname",
    "displayName": "AccountName"
  }, {
    "visible": true,
    "fieldName": "email",
    "displayName": "Email",
  }, {
    "visible": true,
    "fieldName": "mobile",
    "displayName": "Mobile"
  }, {
    "visible": false,
    "fieldName": "userid",
    "displayName": "UserId"
  }, {
    "visible": true,
    "fieldName": "acdate",
    "displayName": "AccountDate"
  }]
  balance
  ngOnInit(): void {
    this.getUserList();
    this.keys = this.headerSettings
  }

  manage(data: any) {
    console.log("Manage data : " + data);
  }

  register(myModal, signUpForm) {
    this.user.appuser.email = this.user.emailid
    this.registerUser(this.user,myModal, signUpForm);
   
  }

  getUserList() {
    this.userService.getUsers().subscribe(data => {
      if (Array.isArray(data))
        this.users = data
    }, err => console.log(err))
  }
  registerUser(user,myModal, signUpForm){
    this.authService.registerUser(user).subscribe(res=>
      {
        if(Array.isArray(res)){
          var error = res[0]
          var code = error["code"]
          this.errorResponse = true
          this.errorMessage = code
          this.user.emaild = "";
          this.user.appuser.email = "";
          
        }else{
          this.errorResponse = true
          this.errorMessage = "Account Created"
          this.getUserList()
        }
         // myModal.hide()
          signUpForm.resetForm();        
      },err=> console.log(err))
  }
  isNumber(val) { return typeof val === 'number'; }
  sort(property) {
    this.isDesc = !this.isDesc; //change the direction    
    this.column = property;
    this.direction = this.isDesc ? 1 : -1;
  };

}
