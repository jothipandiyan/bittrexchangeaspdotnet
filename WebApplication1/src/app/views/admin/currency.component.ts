import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService, AdminService } from 'app/services';

@Component({
  selector: 'currency',
  templateUrl: './currency.component.html'
})
export class CurrencyComponent implements OnInit {
 edit = false
  user: any ={ };
  error;
  currencies
  keys
  currencyHeaderSettings=   [{
    "visible": false,
    "fieldName": "currencyid",
    "displayName": "CurrencyId",
    "type":"Id"
  }, {
    "visible": true,
    "fieldName": "currencyname",
    "displayName": "Name",
    "type":"CurrencyName"
  }, {
    "visible": true,
    "fieldName": "currencyshortname",
    "displayName": "Symbol",
    "type":"String"
  }, {
    "visible": false,
    "fieldName": "cursymbolurl",
    "displayName": "cursymbolurl",
    "type":"none"
  }, {
    "visible": false,
    "fieldName": "curapiurl",
    "displayName": "curapiurl",
    "type":"none"
  }, {
    "visible": true,
    "fieldName": "bidperct",
    "displayName": "BID %",
    "type":"percent",
    "edit":true
  }, {
    "visible": true,
    "fieldName": "askperct",
    "displayName": "ASK %",
    "type":"percent",
    "edit":true
  }, {
    "visible": true,
    "fieldName": "canprimary",
    "displayName": "Primary",
    "type":"primary",
    "edit":true
  }, {
    "visible": true,
    "fieldName": "createdate",
    "displayName": "Create DateTime",
    "type":"dateTime"
  } ,{
    "visible": true,
    "fieldName": "datetimeofupdate",
    "displayName": "Modified DateTime",
    "type":"dateTime"
  }]
  constructor( private router: Router, private adminApi:AdminService ) { }
  radioModel
  onChange(event){
    console.log(event)
    console.log(this.currencies)
  }
  ngOnInit(): void {
    this.getCurrencies()  
    this.keys = this.currencyHeaderSettings
  }

  getCurrencies(){
    this.adminApi.getCurrencyList().subscribe(data =>{
      this.currencies = data
    },err=>console.log(err),()=>console.log("completed"));
  }


  primaryChange(currency, event){
    let checked  = event.target.checked
    this.adminApi.setCurrencyPrimary(currency.currencyid)
    .subscribe(data =>{
      console.log(data)
    },error=>console.log(error))
  }
}
