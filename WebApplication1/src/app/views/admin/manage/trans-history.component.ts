import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AdminService } from 'app/services';

@Component({
  selector: 'trans-history',
  templateUrl: './trans-history.component.html'
})
export class TransHistoryComponent implements OnInit {

  buySellheaderSettings = [{
    "visible": false,
    "fieldName": "secondcurid",
    "displayName": "secondcurid"
  }, {
    "visible": true,
    "fieldName": "units",
    "displayName": "Units"
  }, {
    "visible": true,
    "fieldName": "Total",
    "displayName": "Total"  
  }, {
    "visible": true,
    "fieldName": "rate",
    "displayName": "Rate"
  }, {
    "visible": true,
    "fieldName": "sum",
    "displayName": "SUM"
  }]
  accountId
  queryParamSubscriber$
  transactionHistory

  constructor( private router: Router,private activatedRoute:ActivatedRoute, private admin:AdminService) { }

  ngOnInit(): void {
    this.queryParamSubscriber$ = this.activatedRoute.queryParams.subscribe(params => {
      this.accountId = params['accountId'] || null;
      this.getUserTransactionHistory(this.accountId)

    });
  }
  ngOnDestroy() {
    if(this.queryParamSubscriber$)
      this.queryParamSubscriber$.unsubscribe();
  }
  getUserTransactionHistory(accountId){
    this.admin.getUserTransactionHistory(accountId).subscribe(res=>{
      if(Array.isArray(res))  
        this.transactionHistory = res
    })
  }


}
