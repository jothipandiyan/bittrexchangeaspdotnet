import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'reset-pwd',
  templateUrl: './reset-pwd.component.html'
})
export class ResetPasswordComponent implements OnInit {
 
  model: any = { };
  error;
  constructor( private router: Router ) { }

  ngOnInit(): void {
  }
  
  save()
  {
    this.error = null;
    if(this.model.password === this.model.confirmPassword)
    {
      console.log("Changed Successfully");
    }
    else{
      this.error = "New Password and Confirm Password should be Same";
    }
  }
}
