import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'app/services';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'admin-wallet',
  templateUrl: './admin-wallet.component.html'
})
export class AdminWalletComponent implements OnInit {

  user: any = {};
  currencies
  constructor(private router: Router, private adminApi: AdminService, private activatedRoute: ActivatedRoute) { }
  queryParamSubscriber$
  errorResponse
  errorMessage
  ngOnDestroy() {
    if (this.queryParamSubscriber$)
      this.queryParamSubscriber$.unsubscribe();
  }
  keys
  accountId
  userInfo
  headerSettings = [{
    "visible": false,
    "fieldName": "walletacid",
    "displayName": "WalletAccountId"
  }, {
    "visible": false,
    "fieldName": "accountid",
    "displayName": "AccountId"
  }, {
    "visible": false,
    "fieldName": "currencyid",
    "displayName": "CurrencyId",
  }, {
    "visible": true,
    "fieldName": "currencyname",
    "displayName": "Currency"
  }, {
    "visible": true,
    "fieldName": "walletaddress",
    "displayName": "WalletAddress"
  }, {
    "visible": true,
    "fieldName": "publickey",
    "displayName": "PublicKey"
  },
  {
    "visible": true,
    "fieldName": "email",
    "displayName": "Email"
  }, {
    "visible": true,
    "fieldName": "fundslocked",
    "displayName": "FundsLocked"
  }, {
    "visible": true,
    "fieldName": "walletguid",
    "displayName": "WalletGuid"
  }, {
    "visible": false,
    "fieldName": "cursymbolurl",
    "displayName": "CurrencySymbolURL"
  }]
  userWallets
  balance = { Units: "", usdrate: "", usdAmount:""}
  ngOnInit(): void {
    //    this.getCurrencies();
    this.keys = this.headerSettings
    this.queryParamSubscriber$ = this.activatedRoute.queryParams.subscribe(params => {
      this.accountId = params['accountId'] || null;
      this.user.AccountID = this.accountId
      this.getWallets();
      this.getAccountInfo()
    });
  }
  filteredCurrencies
  init() {
    var wallets = this.userWallets
    this.filteredCurrencies = []
    for (var currencyIndex in this.currencies) {
      var flag = true
      var currency = this.currencies[currencyIndex]
      for (var index in wallets) {
        var wallet = wallets[index]
        if (wallet.currencyid == currency.currencyid) {
          flag = false
          break;
        }

      }
      if (flag) {
        this.filteredCurrencies.push(currency)
      }

    }

  }
  getCurrencies() {
    this.adminApi.getCurrencyList().subscribe(data => {
      if (Array.isArray(data)) {
        this.currencies = data
        this.init()
      }
    });
  }

  getWallets() {
    this.adminApi.getWalletsPerUser(this.accountId).subscribe(data => {
      if (Array.isArray(data)) {
        this.userWallets = data
        this.getCurrencies();
      }
    }, err => console.log(err), () => console.log("completed"));
  }
  getAccountInfo() {
    this.adminApi.getUsersByAccountId(this.accountId).subscribe(data => {
      if (Array.isArray(data)) {
        this.userInfo = data[0]
      }
    }, err => console.log(err), () => console.log("completed"));
  }

  save(createWalletForm) {
    var data = this.user;
    this.adminApi.createGenericWallet(data)
      .subscribe(data => {
        createWalletForm.resetForm();
        this.errorResponse = true
        this.errorMessage = "Wallet Created"
        this.getWallets();
        setTimeout(()=>this.user.AccountID = this.accountId,0)

      }, err => console.log(data))

    this.init()
  }
  addBalance(balanceModal,addBalaceForm) {
    var data = this.balance

    this.adminApi.addBalance(data)
      .subscribe(data => {
          this.balance = { Units: "", usdrate: "", usdAmount: "" }
          addBalaceForm.resetForm();

        setTimeout(()=>{
          balanceModal.hide()
        },0)

        

      });
  }
  isNumber(val) { return typeof val === 'number'; }

  selectedCurrency(currency: any) {
    this.user.CurrencyId = currency['currencyid'];
  }
userWalletBalance
  getBalance(walletguid){
    this.adminApi.getWalletBalance(walletguid).subscribe(data=>{
      this.userWalletBalance = data
    })
  }
}
