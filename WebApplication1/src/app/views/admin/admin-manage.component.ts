import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'admin-manage',
  templateUrl: './admin-manage.component.html',
  styles:[`
`]
})
export class AdminManageComponent implements OnInit {
  accountId
  constructor( private router: Router,private activatedRoute:ActivatedRoute) { }
  queryParamSubscriber$
  ngOnInit(): void {
    this.queryParamSubscriber$ = this.activatedRoute.queryParams.subscribe(params => {
      this.accountId = params['accountId'] || null;
    });
  }
  ngOnDestroy() {
    if(this.queryParamSubscriber$)
      this.queryParamSubscriber$.unsubscribe();
  }
}
