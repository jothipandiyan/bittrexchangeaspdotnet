import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminManageComponent, UserListComponent, CurrencyComponent } from './index';
import { AdminWalletComponent, ResetPasswordComponent, BidAskComponent, TransHistoryComponent } from './manage/index';
import { AuthGuard } from '../../services/auth.guard';

const routes: Routes = [
  {  path: '',   component: UserListComponent,  data: {  title: 'Markets'  }  },
  {  path: 'users',   component: UserListComponent,  data: {  title: 'Markets'  }, canActivate:[AuthGuard]  },
  {  path: 'currencies',   component: CurrencyComponent,  data: {  title: 'Markets'  }, canActivate:[AuthGuard]  },
  // {  path: 'genericWallet',   component: GenericWallet,  canActivate:[AuthGuard]  },
  {  path: 'manage',   component: AdminManageComponent,  data: {  title: 'Markets'  }, canActivate:[AuthGuard], 
      children:[{ path:'wallet', component: AdminWalletComponent },
      { path:'reset', component: ResetPasswordComponent },
      { path:'trans', component: TransHistoryComponent },
      { path:'bidask', component: BidAskComponent },
      ] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
