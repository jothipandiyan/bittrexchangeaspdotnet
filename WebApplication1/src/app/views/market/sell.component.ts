import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService, UserService, ApiService } from '../../services/index';
import { Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Output } from '@angular/core';

@Component({
    selector: 'sell',
    templateUrl: './sell.component.html'
})
export class SellComponent implements OnInit {
    @Input() currencyData;
    @Input() primaryCurrency
    @Input() secondaryCurrency
    @Input() primaryCurrencyBalance
    @Input() secondaryCurrencyBalance
    @Output() invokeHttpEvent: EventEmitter<any> = new EventEmitter();
    isConditionalBuy = false;
    isConditionalSell = false;
    sellDrop = 'Limit';
    sellCondtion = 'Condition';
    sellForm: FormGroup
    sellFormWithCommision: FormGroup
    sellCommision = 0.02

    constructor(private auth: AuthenticationService, private route: ActivatedRoute,
        private api: ApiService, private formBuilder: FormBuilder, private userService: UserService) {

        this.sellForm = this.formBuilder.group({
            "MarketShort": [''],
            'Units': ['0.0000000', Validators.required],
            'rate': ['0.0000000', [Validators.required]]
        });

        this.sellFormWithCommision = this.formBuilder.group({
            "MarketShort": [''],
            'Units': ['0.0000000', Validators.required],
            'rate': ['0.0000000', [Validators.required]],
            "Commision": ['0.0000000', [Validators.required]],
            "SubTotal": ['0.0000000', [Validators.required]],
            "Total": ['0.0000000', [Validators.required]]
        });


    }

    marketName;
    isLogin = false;

    ngOnInit(): void {
        this.isLogin = this.auth.isLogin;
    }


    subTotal() {
        var units = this.sellForm.controls['Units'].value
        var rate = this.sellForm.controls['rate'].value
        return units * rate
    }
    commision() {

        return this.subTotal() * this.sellCommision;
    }
    total() {
        var subTotal = this.subTotal()
        var commision = this.commision()

        return subTotal - commision;
    }


    ask() {
        if (this.sellForm.dirty && this.sellForm.valid) {
            this.sellForm.controls['MarketShort'].setValue(this.currencyData);

            this.userService.postAsk(this.sellForm.value).subscribe(data => {

                if (data == "0") {
                    this.invokeHttpEvent.emit("sell");
                }
                this.sellForm.reset();

            }, err => console.log(err));
        }

    }

    getLastAskPrice() {
        this.userService.getLastAskPriceByMarket(this.currencyData).subscribe(data => {
            if (typeof data == 'number') {
                var rate = data
                this.sellForm.controls['rate'].setValue(rate);
            }

        });
    }
    getLastBidPrice() {
        this.userService.getLastBidPriceByMarket(this.currencyData).subscribe(data => {
            if (typeof data == 'number') {
                var rate = data
                this.sellForm.controls['rate'].setValue(rate);
            }

        });
    }

    getLastPrice() {
        this.userService.getLastPriceByMarket(this.currencyData).subscribe(data => {
            if (typeof data == 'number') {
                var rate = data
                this.sellForm.controls['rate'].setValue(rate);

            }
        });
    }
}
