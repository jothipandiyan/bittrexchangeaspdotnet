import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService, UserService, ApiService } from 'app/services/index';
import { Input } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Output } from '@angular/core';

@Component({
    selector: 'buy',
    templateUrl: './buy.component.html'
})
export class BuyComponent implements OnInit {
    @Input() currencyData;
    @Input() primaryCurrency
    @Input() secondaryCurrency
    @Input() primaryCurrencyBalance
    @Input() secondaryCurrencyBalance
    @Output() invokeHttpEvent: EventEmitter<any> = new EventEmitter();
    isConditionalBuy = false;
    isConditionalSell = false;
    buyDrop = 'Limit';
    sellDrop = 'Limit';
    buyCondtion = 'Condition';
    buyFormWithCommision: FormGroup
    buyForm: FormGroup
    buyCommision = 0.02
    constructor(private auth: AuthenticationService, private route: ActivatedRoute,
        private api: ApiService, private formBuilder: FormBuilder, private userService: UserService) {
        this.buyForm = this.formBuilder.group({
            "MarketShort": [''],
            'Units': ['0.0000000', Validators.required],
            'rate': ['0.0000000', [Validators.required]]
        });
        this.buyFormWithCommision = this.formBuilder.group({
            "MarketShort": [''],
            'Units': ['0.0000000', Validators.required],
            'rate': ['0.0000000', [Validators.required]],
            "Commision": ['0.0000000', [Validators.required]],
            "SubTotal": ['0.0000000', [Validators.required]],
            "Total": ['0.0000000', [Validators.required]]
        });



    }

    marketName;
    isLogin = false;

    ngOnInit(): void {
        this.isLogin = this.auth.isLogin;
    }
    subTotal() {
        var units = this.buyForm.controls['Units'].value
        var rate = this.buyForm.controls['rate'].value
        return units * rate
    }
    commision() {

        return this.subTotal() * this.buyCommision;
    }
    total() {
        var subTotal = this.subTotal()
        var commision = this.commision()

        return subTotal + commision;
    }

    postBid() {
        if (this.buyForm.dirty && this.buyForm.valid) {
            this.buyForm.controls['MarketShort'].setValue(this.currencyData);

            this.userService.postBid(this.buyForm.value).subscribe(data => {
                this.buyForm.reset();
                if (data == "0") {
                    this.invokeHttpEvent.emit("buy");
                }
            }, err => console.log(err))
        }
    }


    getLastAskPrice() {
        this.userService.getLastAskPriceByMarket(this.currencyData).subscribe(data => {
            if (typeof data == 'number') {
                var rate = data
                this.buyForm.controls['rate'].setValue(rate);
            }

        });
    }
    getLastBidPrice() {
        this.userService.getLastBidPriceByMarket(this.currencyData).subscribe(data => {
            if (typeof data == 'number') {
                var rate = data
                this.buyForm.controls['rate'].setValue(rate);
            }
        });
    }

    getLastPrice() {
        this.userService.getLastPriceByMarket(this.currencyData).subscribe(data => {
            if (typeof data == 'number') {
                var rate = data
                this.buyForm.controls['rate'].setValue(rate);
            }

        });
    }

}
