import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WalletComponent } from './wallet.component';
import { OrderComponent } from './order.component';
import { ProfileComponent } from 'app/views/user/profile/profile.component';
import { AuthGuard } from 'app/services';

const routes: Routes = [
  { path: '' ,  redirectTo: '/markets', pathMatch: 'full'},
  {path:'wallet', component: WalletComponent, data: { title: 'Markets' },canActivate: [AuthGuard]},
  { path: 'order', component: OrderComponent, data: { title: 'Order' },canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent,canActivate: [AuthGuard] },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
