import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { BaseRequestOptions } from '@angular/http';

import { AuthenticationService, UserService, AuthGuard } from 'app/services/index';

import { WalletComponent } from './wallet.component';
import { OrderComponent } from './order.component';

import { UserRoutingModule } from './user-routing.module';
import { ProfileComponent } from 'app/views/user/profile/profile.component';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    UserRoutingModule,SharedModule,
    ChartsModule,
    FormsModule,
    CommonModule,
    HttpModule,
  ],
  declarations: [ WalletComponent, OrderComponent,ProfileComponent ],
  providers: [BaseRequestOptions],
})
export class UserModule { }
