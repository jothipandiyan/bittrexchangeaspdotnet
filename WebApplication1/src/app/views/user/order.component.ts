import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService, ApiService } from 'app/services';

@Component({
  selector: 'order',
  templateUrl: './order.component.html'
})
export class OrderComponent implements OnInit {
 
    inputName;
    edit;
    dataJSON =[{ "Market":"BTC-ZEN", "Currency":"Zencash", "Volume":"217.043", "Changes":"-11.0", "LastPrice": "0.00279140", "High":"0.00318628", "Low":"0.00269720", "Spread":"0.6%", "Added":"06/05/2017", "img":"bitcoin.png", "Status":"Biggest % Gain" }];
  openOrderKeys
  closedOrderKeys
    openOrdersHeaderSettings=   [{
      "visible": false,
      "fieldName": "ID",
      "displayName": "ID",
      "type":"Id"
    }, {
      "visible": true,
      "fieldName": "CurrencyName",
      "displayName": "Currency",
      "type":"String"
    }, {
      "visible": true,
      "fieldName": "rate",
      "displayName": "Rate",
      "type":"String"
    }, {
      "visible": true,
      "fieldName": "units",
      "displayName": "Units",
      "type":"String"
    }, {
      "visible": true,
      "fieldName": "datetime",
      "displayName": "DateTime",
      "type":"dateTime"
    }, {
      "visible": true,
      "fieldName": "actualrate",
      "displayName": "Actual Rate",
      "type":"String",
      "edit":false
    }, {
      "visible": true,
      "fieldName": "buyask",
      "displayName": "Buy / Ask",
      "type":"String",
      "edit":false
    }]
    closedOrdersHeaderSettings=   [{
      "visible": false,
      "fieldName": "ID",
      "displayName": "ID",
      "type":"Id"
    }, {
      "visible": true,
      "fieldName": "CurrencyName",
      "displayName": "Currency",
      "type":"String"
    }, {
      "visible": true,
      "fieldName": "rate",
      "displayName": "Rate",
      "type":"String"
    }, {
      "visible": true,
      "fieldName": "units",
      "displayName": "Units",
      "type":"String"
    }, {
      "visible": true,
      "fieldName": "datetime",
      "displayName": "DateTime",
      "type":"dateTime"
    }, {
      "visible": true,
      "fieldName": "datetimeofsuccess",
      "displayName": "DateTime of Success",
      "type":"dateTime",
      "edit":false
    }, {
      "visible": true,
      "fieldName": "buyask",
      "displayName": "Buy / Ask",
      "type":"String",
      "edit":false
    }]

    openOrders
    closedOrders
  constructor( private router: Router ,private api:UserService) { }

  ngOnInit(): void {
    this.getOpenOrders();
    this.getClosedOrders();
    this.openOrderKeys = this.openOrdersHeaderSettings
    this.closedOrderKeys = this.closedOrdersHeaderSettings

  }

  getOpenOrders(){
    this.api.getOpenOrders().subscribe(data=>{
      if(Array.isArray(data)){
        this.openOrders = data

      }
    })
 
  }
  getClosedOrders(){
    this.api.getClosedOrders().subscribe(data=>{
      if(Array.isArray(data)){
        this.closedOrders = data

      }
    })
 
  }
  FilterByName() {
  }
}
