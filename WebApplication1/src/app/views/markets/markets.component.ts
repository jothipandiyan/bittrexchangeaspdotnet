import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgxCarousel, NgxCarouselStore } from 'ngx-carousel';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import { ApiService } from '../../services/index'
import { Observable } from 'rxjs';
@Component({
  selector: 'markets',
  templateUrl: './markets.component.html'
})
export class MarketsComponent implements OnInit {


  @ViewChild('slickModal') slickModal;
  slideConfig = { 'slidesToShow': 4, "slidesToScroll": 1, "infinite": true, "autoplay": true, "autoplaySpeed": 3000, "pauseOnHover": false, "pauseOnFocus": false };
  imageSlider_interval: any;
  slider_length: number = 0;
  current_slide: number = 0;
  currentSlide
  timerSubscription;

  images = {
    "BTR": "../../../assets/img/logo/btr.png",
    "BTC": "../../../assets/img/logo/btc.png",
    "ETH": "../../../assets/img/logo/eth.png",
    "UBQ": "../../../assets/img/logo/ubq.png",
    "LTC": "../../../assets/img/logo/ltc.png",
    "BLK": "../../../assets/img/logo/blk.png",
    "DASH": "../../../assets/img/logo/dash.png",
    "GAM": "../../../assets/img/logo/gam.png",
    "GRC": "../../../assets/img/logo/grc.png",
    "NEO": "../../../assets/img/logo/neo.png",
    "XRP": "../../../assets/img/logo/xrp.png",
    "XLM": "../../../assets/img/logo/xlm.png"
  }
  dashBoardHeader
  dashBoardHeader1 = [{
    currencyshortname: "BTR",
    currencyname: "BitEther",
    volume: 12,
    change: 10,
    img: "../../../assets/img/logo/btr.png"
  }, {
    currencyshortname: "ETH",
    currencyname: "Ethereum",
    volume: 12,
    change: 10,
    img: "../../../assets/img/logo/eth.png"
  }, {
    currencyshortname: "BTR",
    currencyname: "BitEther",
    volume: 12,
    change: 10,
    img: "../../../assets/img/logo/btr.png"
  }, {
    currencyshortname: "ETH",
    currencyname: "Ethereum",
    volume: 12,
    change: 10,
    img: "../../../assets/img/logo/eth.png"
  }, {
    currencyshortname: "BTR",
    currencyname: "BitEther",
    volume: 12,
    change: 10,
    img: "../../../assets/img/logo/btr.png"
  }]
  btrMarketData
  btcMarketData
  headerSettings = [{
    "visible": false,
    "fieldName": "currencyid",
    "displayName": "CURRENCYID"
  }, {
    "visible": false,
    "fieldName": "againstcurrid",
    "displayName": "AGAINSTCURRID"
  }, {
    "visible": true,
    "fieldName": "curshortnames",
    "displayName": "Market",
    "routing": true,
    "routingURL": "/market"
  }, {
    "visible": true,
    "fieldName": "currencyname",
    "displayName": "CURRENCY"
  }, {
    "visible": true,
    "fieldName": "volume",
    "displayName": "VOLUME"
  }, {
    "visible": true,
    "fieldName": "rate",
    "displayName": "RATE"
  }, {
    "visible": true,
    "fieldName": "hoursmax",
    "displayName": "HOURS MAX"
  }, {
    "visible": true,
    "fieldName": "hoursmin",
    "displayName": "HOURS MIN"
  }]

    
  ngAfterContentInit() {
    if (this.dashBoardHeader) {
      this.currentSlide = this.dashBoardHeader[0]
      //this.slider_length = this.slickModal.slides;
    }
  }
  afterChangedFunction(event) {
    let currentSlideIndex = event.currentSlide;
    if (currentSlideIndex) {
      this.currentSlide = this.dashBoardHeader[currentSlideIndex]
    }

  }


  constructor(private router: Router, private api: ApiService) {

  }

  ngOnInit(): void {
    this.getMarketData("BTC")
    this.getMarketData("BTR")
    this.getMarketsDashboard();
    this.subscribeToData();

  }

  getMarketData(market) {
    this.api.getMarket(market).subscribe(data => {
      if (Array.isArray(data)) {

      if (market == "BTR")
        this.btrMarketData = data

      if (market == "BTC")
        this.btcMarketData = data
      }
    }, err => console.log(err), () => console.log("completed"));


  }
  currencyRoute(data: any) {
    console.log("Routing currency : " + data);
  }
  getMarketsDashboard() {
    this.api.getDashboardHeader().subscribe(datas => {
      if (Array.isArray(datas)) {
        if (datas.length > 0) {
          datas.map(data => {
            data['img'] = this.images[data['currencyshortname']]
            return data;
          })
          if (datas.length == 2) {
            datas.push(datas[0])
            datas.push(datas[1])
            datas.push(datas[0])
          } else if (datas.length == 3) {
            datas.push(datas[0])
            datas.push(datas[1])

          } else if (datas.length == 3) {
            datas.push(datas[0])
            datas.push(datas[0])

          }else if (datas.length == 4) {
            datas.push(datas[0])

          }
          this.dashBoardHeader = datas
          this.currentSlide = this.dashBoardHeader[0]
          console.log(datas)
        }
      }
    }, err => console.log(err), () => console.log("completed"));
  }

  onmoveFn(ev: any) {
    console.log(ev);
  }

  refreshData() {
      this.getMarketData("BTC")
      this.getMarketData("BTR")
      this.getMarketsDashboard();
  }
  ngOnDestroy() {
      if (this.timerSubscription) {
          this.timerSubscription.unsubscribe();
      }
  }

  subscribeToData() {
      this.timerSubscription = Observable.interval(5000).subscribe(() => this.refreshData());
  }
}
