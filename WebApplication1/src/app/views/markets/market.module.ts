import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { BaseRequestOptions } from '@angular/http';
import { NgxCarouselModule } from 'ngx-carousel';
import 'hammerjs';
import { AuthenticationService, UserService, AuthGuard } from 'app/services/index';

import { MarketsComponent } from './index';

import { MarketRoutingModule } from './market-routing.module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from 'app/shared/shared.module';

@NgModule({
  imports: [
    MarketRoutingModule,SharedModule,  
    ChartsModule,
    FormsModule,ReactiveFormsModule,
    CommonModule,
    HttpModule,ModalModule,
    NgxCarouselModule
  ],
  declarations: [ MarketsComponent ],
  providers: [BaseRequestOptions],
})
export class MarketModule { }
