import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, UserService } from '../../services/index';
export interface FormModel {
    captcha?: string;
}

@Component({
    selector: 'register',
    templateUrl: './register.component.html',
})
export class RegisterComponent implements OnInit {

    public formModel: FormModel = {};

    model: any = {};
    error ;
    
    constructor(
        private router: Router, private userService: UserService,
        private authenticationService: AuthenticationService) { }

    ngOnInit() {
    }

    user: any = { "acname": "", "emailid": "", "mobile": "", "appuser": { "email": "", "Password": "" } };
    errorResponse
    errorMessage

    register(signUpForm) {
        this.user.appuser.email = this.user.emailid
        this.registerUser(this.user, signUpForm);

    }

    registerUser(user, signUpForm) {
        this.authenticationService.registerUser(user).subscribe(res => {
            if (Array.isArray(res)) {
                var error = res[0]
                var code = error["code"]
                this.errorResponse = true
                this.errorMessage = code
                this.user.emaild = "";
                this.user.appuser.email = "";

            } else {
                this.errorResponse = true
                this.errorMessage = "Account Created"
                this.error = "Registration completed, please verify your email and login"
            }
            // myModal.hide()

            this.user = { "acname": "", "emailid": "", "mobile": "", "appuser": { "email": "", "Password": "" } };
            if (signUpForm) {
                console.log(signUpForm)
                signUpForm.resetForm();
                if (signUpForm.form) {
                    signUpForm.form.markAsPristine();
                    signUpForm.form.markAsUntouched();
                    signUpForm.form.updateValueAndValidity();
                }
            }
        }, err => console.log(err))
    }
}
