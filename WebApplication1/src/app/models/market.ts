export class Market {
   constructor(
    public currencyid :number,
    public againstcurrid :number,
    public currencyname :string,
    public curshortnames :string,
    public volume :number,
    public rate :number,
    public  hoursmax :string,
    public hoursmin:string 
   ){

   }
}

export class Product {
   constructor(
    public id : number,    
    public name : string,    
    public description : string
   ){

   }
}