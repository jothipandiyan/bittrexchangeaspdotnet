import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';

function _window():any  {
  console.log(navigator)
  console.log(window)
  console.log((<any>window).navigator)

  // return the global native browser window object
   return window;
}

@Component({
  // tslint:disable-next-line
  selector: 'body',
  templateUrl: './app.component.html'
})
export class AppComponent {

  ngAfterViewInit() {
    var connection =(<any>navigator).connection
    connection.onchange = this.test 
    console.log(connection)
    Observable.fromEvent(window,'online').subscribe(res=>{
      console.log(res)
    })
    Observable.fromEvent(window,'offline').subscribe(res=>{
      console.log(res)
    })

  }
  test(event){
    console.log(event)
  }
}
