webpackJsonp(["user.module"],{

/***/ "../../../../../src/app/views/user/order.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <div class=\"panel\">\r\n        <div class=\"panel-heading\">\r\n            OPEN\r\n            <div class=\"panel-action\">\r\n                <form class=\"form-inline \">\r\n                    <span class=\"form-group row \">\r\n                        <label>Filter:</label>\r\n                        <input type=\"text\" class=\"form-control\" name=\"search\" [(ngModel)]=\"inputName\" (change)=\"FilterByName()\">\r\n                    </span>\r\n                </form>\r\n            </div>\r\n        </div>\r\n        <div class=\"table-responsive\">\r\n            <table class=\"table table-striped orders\">\r\n                <thead>\r\n                    <tr>\r\n                        <th class=\"col-header th-header\">\r\n                            <span># </span>&nbsp;\r\n                        </th>\r\n                        <th *ngFor=\"let item of openOrderKeys\" [hidden]=\"!item.visible\" class=\"col-header th-header\">\r\n                            <span>{{ item.displayName }} </span>\r\n                        </th>\r\n                        <th class=\"col-header th-header\">\r\n                            <span class=\"glyphicon glyphicon-remove\" style=\"color:white\"></span>\r\n                        </th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody>\r\n                    <tr *ngFor=\"let order of openOrders; let i = index\" style=\"text-align:center\">\r\n                        <td> {{ i + 1}}\r\n                        </td>\r\n                        <td *ngFor=\"let item of openOrderKeys\" [hidden]=\"!item.visible\" style=\"text-align:center\">\r\n                            <div *ngIf=\"item.type == 'CurrencyName'\">\r\n                                <img [src]=\"order.cursymbolurl\" style=\"float:left\" class=\"currrecy-logo\">\r\n                                <span>\r\n                                    {{ order[ item.fieldName] }}\r\n                                </span>\r\n                            </div>\r\n                            <div *ngIf=\"item.type == 'String'\">\r\n                                {{ order[ item.fieldName] }}\r\n                            </div>\r\n                            <div *ngIf=\"item.type == 'percent'\">\r\n                                <input type=\"number\" [name]=\"item.fieldName\" [(ngModel)]=\"order[ item.fieldName]\" *ngIf=\"edit\" />\r\n                                <span *ngIf=\"!edit\"> {{ order[ item.fieldName] || 0}}</span>\r\n                            </div>\r\n                            <div *ngIf=\"item.type == 'primary'\">\r\n                                <span *ngIf=\"edit\">\r\n                                    <!-- <ui-switch (change)=\"onChange($event)\" [(ngModel)]=\"order[ item.fieldName]\" [checked]=\"order[ item.fieldName] == 1\"></ui-switch> -->\r\n                                    <label class=\"switch switch-3d switch-primary\">\r\n                                        <input checked=\"\" class=\"switch-input\" type=\"checkbox\" (change)=\"onChange($event)\" [(ngModel)]=\"order[ item.fieldName]\" [checked]=\"order[ item.fieldName] == 1\">\r\n                                        <span class=\"switch-label\"></span>\r\n                                        <span class=\"switch-handle\"></span>\r\n                                    </label>\r\n                                </span>\r\n                                <span *ngIf=\"!edit\">\r\n                                    {{ order[ item.fieldName] == 1 }}\r\n                                </span>\r\n                            </div>\r\n                            <div *ngIf=\"item.type == 'dateTime'\">\r\n                                {{ order[ item.fieldName] | amUtc | amDateFormat: 'YYYY-MM-DD hh:mm:ss' }}\r\n                            </div>\r\n                        </td>\r\n                        <td>\r\n                            <a class=\"btn btn-primary btn-sm\" #cancel>\r\n                                <span class=\"glyphicon glyphicon-remove\" style=\"color:white\"></span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n\r\n   \r\n    <div >\r\n        <div class=\"panel\">\r\n            <div class=\"panel-heading\">\r\n                Completed\r\n                <div class=\"panel-action\">\r\n                    <form class=\"form-inline \">\r\n                        <span class=\"form-group row \">\r\n                            <label>Filter:</label>\r\n                            <input type=\"text\" class=\"form-control\" name=\"search\" [(ngModel)]=\"inputName\" (change)=\"FilterByName()\">\r\n                        </span>\r\n                    </form>\r\n                </div>\r\n            </div>\r\n            <div class=\"table-responsive\">\r\n                <table class=\"table table-striped orders\">\r\n                    <thead>\r\n                        <tr>\r\n                            <th class=\"col-header th-header\">\r\n                                <span># </span>&nbsp;\r\n                            </th>\r\n                            <th *ngFor=\"let item of closedOrderKeys\" [hidden]=\"!item.visible\" class=\"col-header th-header\">\r\n                                <span>{{ item.displayName }} </span>\r\n                            </th>\r\n                            <th class=\"col-header th-header\" *ngIf=\"edit\">\r\n                                <span>Action </span>\r\n                            </th>\r\n                        </tr>\r\n                    </thead>\r\n                    <tbody>\r\n                        <tr *ngFor=\"let order of closedOrders; let i = index\">\r\n                            <td> {{ i + 1}}\r\n                            </td>\r\n                            <td *ngFor=\"let item of closedOrderKeys\" [hidden]=\"!item.visible\" style=\"text-align:center\">\r\n                                <div *ngIf=\"item.type == 'CurrencyName'\">\r\n                                    <img [src]=\"order.cursymbolurl\" style=\"float:left\" class=\"currrecy-logo\">\r\n                                    <span>\r\n                                        {{ order[ item.fieldName] }}\r\n                                    </span>\r\n                                </div>\r\n                                <div *ngIf=\"item.type == 'String'\">\r\n                                    {{ order[ item.fieldName] }}\r\n                                </div>\r\n                                <div *ngIf=\"item.type == 'percent'\">\r\n                                    <input type=\"number\" [name]=\"item.fieldName\" [(ngModel)]=\"order[ item.fieldName]\" *ngIf=\"edit\" />\r\n                                    <span *ngIf=\"!edit\"> {{ order[ item.fieldName] || 0}}</span>\r\n                                </div>\r\n                                <div *ngIf=\"item.type == 'primary'\">\r\n                                    <span *ngIf=\"edit\">\r\n                                        <!-- <ui-switch (change)=\"onChange($event)\" [(ngModel)]=\"order[ item.fieldName]\" [checked]=\"order[ item.fieldName] == 1\"></ui-switch> -->\r\n                                        <label class=\"switch switch-3d switch-primary\">\r\n                                            <input checked=\"\" class=\"switch-input\" type=\"checkbox\" (change)=\"onChange($event)\" [(ngModel)]=\"order[ item.fieldName]\" [checked]=\"order[ item.fieldName] == 1\">\r\n                                            <span class=\"switch-label\"></span>\r\n                                            <span class=\"switch-handle\"></span>\r\n                                        </label>\r\n                                    </span>\r\n                                    <span *ngIf=\"!edit\">\r\n                                        {{ order[ item.fieldName] == 1 }}\r\n                                    </span>\r\n                                </div>\r\n                                <div *ngIf=\"item.type == 'dateTime'\">\r\n                                    {{ order[ item.fieldName] | amUtc | amDateFormat: 'YYYY-MM-DD hh:mm:ss' }}\r\n                                </div>\r\n                            </td>\r\n                            <td *ngIf=\"edit\">\r\n\r\n                                <a class=\"btn btn-primary btn-sm\" #cancel>\r\n                                    <span class=\"glyphicon glyphicon-remove\" style=\"color:white\"></span>\r\n                                </a>\r\n                            </td>\r\n                        </tr>\r\n                    </tbody>\r\n                </table>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/user/order.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services__ = __webpack_require__("../../../../../src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrderComponent = /** @class */ (function () {
    function OrderComponent(router, api) {
        this.router = router;
        this.api = api;
        this.dataJSON = [{ "Market": "BTC-ZEN", "Currency": "Zencash", "Volume": "217.043", "Changes": "-11.0", "LastPrice": "0.00279140", "High": "0.00318628", "Low": "0.00269720", "Spread": "0.6%", "Added": "06/05/2017", "img": "bitcoin.png", "Status": "Biggest % Gain" }];
        this.openOrdersHeaderSettings = [{
                "visible": false,
                "fieldName": "ID",
                "displayName": "ID",
                "type": "Id"
            }, {
                "visible": true,
                "fieldName": "CurrencyName",
                "displayName": "Currency",
                "type": "String"
            }, {
                "visible": true,
                "fieldName": "rate",
                "displayName": "Rate",
                "type": "String"
            }, {
                "visible": true,
                "fieldName": "units",
                "displayName": "Units",
                "type": "String"
            }, {
                "visible": true,
                "fieldName": "datetime",
                "displayName": "DateTime",
                "type": "dateTime"
            }, {
                "visible": true,
                "fieldName": "actualrate",
                "displayName": "Actual Rate",
                "type": "String",
                "edit": false
            }, {
                "visible": true,
                "fieldName": "buyask",
                "displayName": "Buy / Ask",
                "type": "String",
                "edit": false
            }];
        this.closedOrdersHeaderSettings = [{
                "visible": false,
                "fieldName": "ID",
                "displayName": "ID",
                "type": "Id"
            }, {
                "visible": true,
                "fieldName": "CurrencyName",
                "displayName": "Currency",
                "type": "String"
            }, {
                "visible": true,
                "fieldName": "rate",
                "displayName": "Rate",
                "type": "String"
            }, {
                "visible": true,
                "fieldName": "units",
                "displayName": "Units",
                "type": "String"
            }, {
                "visible": true,
                "fieldName": "datetime",
                "displayName": "DateTime",
                "type": "dateTime"
            }, {
                "visible": true,
                "fieldName": "datetimeofsuccess",
                "displayName": "DateTime of Success",
                "type": "dateTime",
                "edit": false
            }, {
                "visible": true,
                "fieldName": "buyask",
                "displayName": "Buy / Ask",
                "type": "String",
                "edit": false
            }];
    }
    OrderComponent.prototype.ngOnInit = function () {
        this.getOpenOrders();
        this.getClosedOrders();
        this.openOrderKeys = this.openOrdersHeaderSettings;
        this.closedOrderKeys = this.closedOrdersHeaderSettings;
    };
    OrderComponent.prototype.getOpenOrders = function () {
        var _this = this;
        this.api.getOpenOrders().subscribe(function (data) {
            if (Array.isArray(data)) {
                _this.openOrders = data;
            }
        });
    };
    OrderComponent.prototype.getClosedOrders = function () {
        var _this = this;
        this.api.getClosedOrders().subscribe(function (data) {
            if (Array.isArray(data)) {
                _this.closedOrders = data;
            }
        });
    };
    OrderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'order',
            template: __webpack_require__("../../../../../src/app/views/user/order.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_2_app_services__["e" /* UserService */]])
    ], OrderComponent);
    return OrderComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/user/profile/profile.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-2\" style=\"border:1px\">\r\n            <ul class=\"list-group\">\r\n                <li class=\"list-group-item\">Summary</li>\r\n            </ul>\r\n            <hr>\r\n            <ul class=\"list-group\">\r\n                 <li class=\"list-group-item \">Basic Verification</li>\r\n                 <li class=\"list-group-item\">Enhanced Verification</li>\r\n            </ul>\r\n            <hr>\r\n\r\n            <ul class=\"list-group\">\r\n                    <li class=\"list-group-item\">Password</li>\r\n               </ul>\r\n           </div>\r\n        <div class=\"col-md-1 \">\r\n        </div>        \r\n        <div class=\"col-md-8 \">\r\n            <h2>User Activity</h2>\r\n            <table class=\"table table-striped\">\r\n                <thead>\r\n                    <tr>\r\n                        <th>Timestamp</th>\r\n                        <th>Address</th>\r\n                        <th>UserAgent</th>\r\n                        <th>Activity</th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody>\r\n                </tbody>\r\n            </table>\r\n\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/user/profile/profile.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfileComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfileComponent = /** @class */ (function () {
    function ProfileComponent(router) {
        this.router = router;
    }
    ProfileComponent.prototype.ngOnInit = function () {
    };
    ProfileComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'profile',
            template: __webpack_require__("../../../../../src/app/views/user/profile/profile.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], ProfileComponent);
    return ProfileComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/user/user-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__wallet_component__ = __webpack_require__("../../../../../src/app/views/user/wallet.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_component__ = __webpack_require__("../../../../../src/app/views/user/order.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_app_views_user_profile_profile_component__ = __webpack_require__("../../../../../src/app/views/user/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services__ = __webpack_require__("../../../../../src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    { path: '', redirectTo: '/markets', pathMatch: 'full' },
    { path: 'wallet', component: __WEBPACK_IMPORTED_MODULE_2__wallet_component__["a" /* WalletComponent */], data: { title: 'Markets' }, canActivate: [__WEBPACK_IMPORTED_MODULE_5_app_services__["c" /* AuthGuard */]] },
    { path: 'order', component: __WEBPACK_IMPORTED_MODULE_3__order_component__["a" /* OrderComponent */], data: { title: 'Order' }, canActivate: [__WEBPACK_IMPORTED_MODULE_5_app_services__["c" /* AuthGuard */]] },
    { path: 'profile', component: __WEBPACK_IMPORTED_MODULE_4_app_views_user_profile_profile_component__["a" /* ProfileComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_5_app_services__["c" /* AuthGuard */]] },
];
var UserRoutingModule = /** @class */ (function () {
    function UserRoutingModule() {
    }
    UserRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], UserRoutingModule);
    return UserRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/user/user.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts__ = __webpack_require__("../../../../ng2-charts/ng2-charts.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__wallet_component__ = __webpack_require__("../../../../../src/app/views/user/wallet.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__order_component__ = __webpack_require__("../../../../../src/app/views/user/order.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__user_routing_module__ = __webpack_require__("../../../../../src/app/views/user/user-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_app_views_user_profile_profile_component__ = __webpack_require__("../../../../../src/app/views/user/profile/profile.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_app_shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_7__user_routing_module__["a" /* UserRoutingModule */], __WEBPACK_IMPORTED_MODULE_9_app_shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts__["ChartsModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* HttpModule */],
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_5__wallet_component__["a" /* WalletComponent */], __WEBPACK_IMPORTED_MODULE_6__order_component__["a" /* OrderComponent */], __WEBPACK_IMPORTED_MODULE_8_app_views_user_profile_profile_component__["a" /* ProfileComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* BaseRequestOptions */]],
        })
    ], UserModule);
    return UserModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/user/wallet.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"title\" style=\"padding:1vw;\" >\r\n    ACCOUNT BALANCE\r\n</div>\r\n\r\n<div class= \"subtitle\" style=\"padding:1vw;\" >\r\n    Estimated Value : 0.0000000 BTC/0.00 USD;\r\n</div>\r\n\r\n<div style=\"padding:1vw;\" >\r\n    <table class=\"table table-striped\" >\r\n        <thead>\r\n            <tr>\r\n            <th>+</th>\r\n            <th>CURRENCY NAME</th>\r\n            <th>SYMBOL</th>\r\n            <th>AVAILABLE BALANCE</th>\r\n            <th>PENDING DEPOSIT</th>\r\n            <th>RESERVED</th>\r\n            <th>TOTAL</th>\r\n            <th>EST.BTC VALUE</th>\r\n            <th>% CHANGE</th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n            <tr *ngFor='let data of dataJSON'>\r\n            <td [ngStyle]=\"{'color' : 'blue'}\" routerLink=\"/markets/market\" [queryParams]=\"{marketName: data.MarketName }\" >{{data.MarketName}}</td>\r\n            <td>{{data.Currency}}</td>\r\n            <td>{{data.Volume}}</td>\r\n            <td [ngStyle]=\"{ 'color' : (data.Changes < 0) ? 'red' : 'green' }\">{{data.Changes}} % {{ (data.Changes < 0)  ? '&#x25bc;':'&#x25b2;' }} </td>\r\n            <td>{{data.LastPrice}}</td>\r\n            <td>{{data.High}}</td>\r\n            <td>{{data.Low}}</td>\r\n            <td>{{data.Spread}}</td>\r\n            <td>{{data.Added}}</td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n    <ul class=\"pagination\">\r\n        <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Prev</a></li>\r\n        <li class=\"page-item active\">\r\n            <a class=\"page-link\" href=\"#\">1</a>\r\n        </li>\r\n        <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\r\n        <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\r\n        <li class=\"page-item\"><a class=\"page-link\" href=\"#\">4</a></li>\r\n        <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Next</a></li>\r\n    </ul>\r\n</div>\r\n\r\n\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-lg-6\">\r\n        <div class=\"subtitle\" style=\"padding:1vw;\" >\r\n            WITHDRAWALS HISTORY\r\n        </div>\r\n        <div style=\"padding:1vw;\" >\r\n            <table class=\"table table-striped\" >\r\n                <thead>\r\n                    <tr>\r\n                    <th>+</th>\r\n                    <th>DATE</th>\r\n                    <th>CURRENCY</th>\r\n                    <th>UNITS</th>\r\n                    <th>STATUS</th>\r\n                    <th>+</th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody>\r\n                    <tr>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            <div>\r\n                <p>you have no pending withdrawals.</p> \r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div  class=\"col-lg-6\">\r\n        <div class=\"row\" style=\"padding:1vw;\" >\r\n            <div class=\"subtitle col-lg-6\" >\r\n                DEPOSITS HISTORY\r\n            </div>\r\n            <div class=\"col-lg-6\" >\r\n                <span>\r\n                    Display \r\n                    <input type='number' name='completeDisplay'> rows\r\n                </span>\r\n                <!-- <span>\r\n                    Search \r\n                    <input type='text' name='search'>\r\n                </span> -->\r\n            </div>\r\n        </div>\r\n\r\n        <div style=\"padding:1vw;\" >\r\n            <table class=\"table table-striped\" >\r\n                <thead>\r\n                    <tr>\r\n                    <th>+</th>\r\n                    <th>LAST CHECKED</th>\r\n                    <th>CURRENCY</th>\r\n                    <th>UNITS</th>\r\n                    <th>STATUS</th>\r\n                    <th>+</th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody>\r\n                    <tr>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            <div>\r\n                <p>you have no pending deposites.</p>   \r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n    "

/***/ }),

/***/ "../../../../../src/app/views/user/wallet.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalletComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services__ = __webpack_require__("../../../../../src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WalletComponent = /** @class */ (function () {
    function WalletComponent(router, service) {
        this.router = router;
        this.service = service;
        this.dataJSON = [{ "Market": "BTC-ZEN", "Currency": "Zencash", "Volume": "217.043", "Changes": "-11.0", "LastPrice": "0.00279140", "High": "0.00318628", "Low": "0.00269720", "Spread": "0.6%", "Added": "06/05/2017", "img": "bitcoin.png", "Status": "Biggest % Gain" },
            { "Market": "BTC-ZEC", "Currency": "Zcash", "Volume": "712.355", "Changes": "-8.5", "LastPrice": "0.352558", "High": "0.00556525", "Low": "0.02525856", "Spread": "0.7%", "Added": "10/28/2016", "img": "nxt.png", "Status": "Top Volume" },
            { "Market": "BTC-ZCL", "Currency": "Zclassic", "Volume": "186.442", "Changes": "15.6", "LastPrice": "0.0065268", "High": "0.025125", "Low": "0.2022514", "Spread": "0.4%", "Added": "11/16/2016", "img": "zclass.png", "Status": "Biggest % Gain" },
            { "Market": "BTC-XZC", "Currency": "WhiteCoin", "Volume": "2322.776", "Changes": "41.3", "LastPrice": "0.00425156", "High": "0.215205", "Low": "0.0025155", "Spread": "0.2%", "Added": "02/18/2016", "img": "spreadcoin.png", "Status": "Top Volume" },
            { "Market": "BTC-XVG", "Currency": "Verge", "Volume": "33.618", "Changes": "5.7", "LastPrice": "0.0002553", "High": "0.0202155", "Low": "0.0254858", "Spread": "3.8%", "Added": "12/23/2014", "img": "bitcoin.png", "Status": "Top Volume" }];
    }
    WalletComponent.prototype.ngOnInit = function () {
        this.getBalance();
    };
    WalletComponent.prototype.getBalance = function () {
        this.service.getTotalBalance().subscribe(function (data) {
            console.log(data);
        });
    };
    WalletComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'wallet',
            template: __webpack_require__("../../../../../src/app/views/user/wallet.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_2_app_services__["e" /* UserService */]])
    ], WalletComponent);
    return WalletComponent;
}());



/***/ })

});
//# sourceMappingURL=user.module.chunk.js.map