webpackJsonp(["admin.module"],{

/***/ "../../../../../src/app/views/admin/admin-manage.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "../../../../../src/app/views/admin/admin-manage.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminManageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminManageComponent = /** @class */ (function () {
    function AdminManageComponent(router, activatedRoute) {
        this.router = router;
        this.activatedRoute = activatedRoute;
    }
    AdminManageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.queryParamSubscriber$ = this.activatedRoute.queryParams.subscribe(function (params) {
            _this.accountId = params['accountId'] || null;
        });
    };
    AdminManageComponent.prototype.ngOnDestroy = function () {
        if (this.queryParamSubscriber$)
            this.queryParamSubscriber$.unsubscribe();
    };
    AdminManageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'admin-manage',
            template: __webpack_require__("../../../../../src/app/views/admin/admin-manage.component.html"),
            styles: ["\n"]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]])
    ], AdminManageComponent);
    return AdminManageComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/admin/admin-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__index__ = __webpack_require__("../../../../../src/app/views/admin/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__manage_index__ = __webpack_require__("../../../../../src/app/views/admin/manage/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth_guard__ = __webpack_require__("../../../../../src/app/services/auth.guard.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__index__["c" /* UserListComponent */], data: { title: 'Markets' } },
    { path: 'users', component: __WEBPACK_IMPORTED_MODULE_2__index__["c" /* UserListComponent */], data: { title: 'Markets' }, canActivate: [__WEBPACK_IMPORTED_MODULE_4__services_auth_guard__["a" /* AuthGuard */]] },
    { path: 'currencies', component: __WEBPACK_IMPORTED_MODULE_2__index__["b" /* CurrencyComponent */], data: { title: 'Markets' }, canActivate: [__WEBPACK_IMPORTED_MODULE_4__services_auth_guard__["a" /* AuthGuard */]] },
    // {  path: 'genericWallet',   component: GenericWallet,  canActivate:[AuthGuard]  },
    { path: 'manage', component: __WEBPACK_IMPORTED_MODULE_2__index__["a" /* AdminManageComponent */], data: { title: 'Markets' }, canActivate: [__WEBPACK_IMPORTED_MODULE_4__services_auth_guard__["a" /* AuthGuard */]],
        children: [{ path: 'wallet', component: __WEBPACK_IMPORTED_MODULE_3__manage_index__["a" /* AdminWalletComponent */] },
            { path: 'reset', component: __WEBPACK_IMPORTED_MODULE_3__manage_index__["c" /* ResetPasswordComponent */] },
            { path: 'trans', component: __WEBPACK_IMPORTED_MODULE_3__manage_index__["d" /* TransHistoryComponent */] },
            { path: 'bidask', component: __WEBPACK_IMPORTED_MODULE_3__manage_index__["b" /* BidAskComponent */] },
        ] },
];
var AdminRoutingModule = /** @class */ (function () {
    function AdminRoutingModule() {
    }
    AdminRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], AdminRoutingModule);
    return AdminRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/admin/admin.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminModule", function() { return AdminModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts__ = __webpack_require__("../../../../ng2-charts/ng2-charts.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common__ = __webpack_require__("../../../common/esm5/common.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_services_index__ = __webpack_require__("../../../../../src/app/services/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__index__ = __webpack_require__("../../../../../src/app/views/admin/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__manage_index__ = __webpack_require__("../../../../../src/app/views/admin/manage/index.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__admin_routing_module__ = __webpack_require__("../../../../../src/app/views/admin/admin-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ngx_bootstrap_modal__ = __webpack_require__("../../../../ngx-bootstrap/modal/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_app_views_admin_generic_wallet_component__ = __webpack_require__("../../../../../src/app/views/admin/generic-wallet.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_app_interceptor_tokenInterceptor__ = __webpack_require__("../../../../../src/app/interceptor/tokenInterceptor.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common_http__ = __webpack_require__("../../../common/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angular2_moment__ = __webpack_require__("../../../../angular2-moment/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_app_shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var AdminModule = /** @class */ (function () {
    function AdminModule() {
    }
    AdminModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_8__admin_routing_module__["a" /* AdminRoutingModule */], __WEBPACK_IMPORTED_MODULE_14_app_shared_shared_module__["a" /* SharedModule */],
                __WEBPACK_IMPORTED_MODULE_1_ng2_charts_ng2_charts__["ChartsModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["ReactiveFormsModule"],
                __WEBPACK_IMPORTED_MODULE_4__angular_common__["CommonModule"],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_9_ngx_bootstrap_modal__["a" /* ModalModule */], __WEBPACK_IMPORTED_MODULE_13_angular2_moment__["MomentModule"]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_6__index__["a" /* AdminManageComponent */], __WEBPACK_IMPORTED_MODULE_6__index__["b" /* CurrencyComponent */], __WEBPACK_IMPORTED_MODULE_10_app_views_admin_generic_wallet_component__["a" /* GenericWallet */], __WEBPACK_IMPORTED_MODULE_6__index__["c" /* UserListComponent */], __WEBPACK_IMPORTED_MODULE_7__manage_index__["a" /* AdminWalletComponent */], __WEBPACK_IMPORTED_MODULE_7__manage_index__["c" /* ResetPasswordComponent */], __WEBPACK_IMPORTED_MODULE_7__manage_index__["b" /* BidAskComponent */], __WEBPACK_IMPORTED_MODULE_7__manage_index__["d" /* TransHistoryComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* BaseRequestOptions */], __WEBPACK_IMPORTED_MODULE_5_app_services_index__["a" /* AdminService */],
                { provide: __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["a" /* HTTP_INTERCEPTORS */], useClass: __WEBPACK_IMPORTED_MODULE_11_app_interceptor_tokenInterceptor__["a" /* TokenInterceptor */], multi: true }
            ],
        })
    ], AdminModule);
    return AdminModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/admin/currency.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div>\r\n    <div class=\"panel\">\r\n        <div class=\"panel-heading\">\r\n            CURRENCIES\r\n            <div class=\"panel-action\">\r\n                <button class=\"btn btn-success btn-xs\" data-toggle=\"modal\" (click)=\"edit=true;\"> Edit </button>\r\n                <button *ngIf=\"edit\" class=\"btn btn-success btn-xs\" data-toggle=\"modal\" (click)=\"edit=false;\"> Cancel </button>\r\n            </div>\r\n        </div>\r\n        <table class=\"table table-striped table-hover \">\r\n            <thead>\r\n                <tr>\r\n                    <th class=\"col-header th-header\">\r\n                        <span># </span>&nbsp;\r\n                    </th> \r\n                    <th></th>\r\n                    <th *ngFor=\"let item of keys\" [hidden]=\"!item.visible\" class=\"th-header\">\r\n                        <span>{{ item.displayName }} </span>\r\n                    </th>\r\n                    <th class=\"col-header th-header\" *ngIf=\"edit\">\r\n                        <span>Action </span>\r\n                    </th>\r\n                </tr>\r\n            </thead>\r\n            <tbody>\r\n                <tr *ngFor=\"let currency of currencies; let i = index\">\r\n                    <td> {{ i + 1}}\r\n                    </td>\r\n                    <td>\r\n                         <img [src]=\"currency.cursymbolurl\" style=\"float:left\" class=\"currrecy-logo img-circle\" style=\"width:30px;\">\r\n                    </td>\r\n                    <td *ngFor=\"let item of keys\" [hidden]=\"!item.visible\" >\r\n                        <div *ngIf=\"item.type == 'CurrencyName'\">\r\n                                {{ currency[ item.fieldName] }}\r\n                        </div>\r\n                        <div *ngIf=\"item.type == 'String'\">\r\n                            {{ currency[ item.fieldName] }}\r\n                        </div>\r\n                        <div *ngIf=\"item.type == 'percent'\">\r\n                            <input type=\"number\" class=\"form-control\" [name]=\"item.fieldName\" [(ngModel)]=\"currency[ item.fieldName]\" *ngIf=\"edit\" />\r\n                            <span *ngIf=\"!edit\"> {{ currency[ item.fieldName] || 0}}</span>\r\n                        </div>\r\n                        <div *ngIf=\"item.type == 'primary'\">\r\n                            <span *ngIf=\"edit\">\r\n                                <!-- <ui-switch (change)=\"onChange($event)\" [(ngModel)]=\"currency[ item.fieldName]\" [checked]=\"currency[ item.fieldName] == 1\"></ui-switch> -->\r\n                                <label class=\"switch switch-3d switch-primary\">\r\n                                    <input checked=\"\" class=\"switch-input \" type=\"checkbox\" (change)=\"onChange($event)\" [(ngModel)]=\"currency[ item.fieldName]\"\r\n                                        [checked]=\"currency[ item.fieldName] == 1\">\r\n                                    <span class=\"switch-label\"></span>\r\n                                    <span class=\"switch-handle\"></span>\r\n                                </label>\r\n                            </span>\r\n                            <span *ngIf=\"!edit\">\r\n                                {{ currency[ item.fieldName] == 1 }}\r\n                            </span>\r\n                        </div>\r\n                        <div *ngIf=\"item.type == 'dateTime'\">\r\n                            {{ currency[ item.fieldName] | amUtc | amDateFormat: 'YYYY-MM-DD hh:mm:ss' }}\r\n                        </div>\r\n                    </td>\r\n                    <td *ngIf=\"edit\">\r\n\r\n                        <a class=\"btn btn-primary btn-sm\" #cancel>\r\n                            <span class=\"glyphicon glyphicon-remove\" style=\"color:white\"></span>\r\n                        </a>\r\n                    </td>\r\n                </tr>\r\n            </tbody>\r\n        </table>\r\n    </div>\r\n\r\n</div>\r\n\r\n<!-- <app-table [records]=currencies></app-table> -->\r\n<!-- <div class=\"animated fadeIn\">\r\n    <div class=\"row\">\r\n        <div class=\"col-6 col-lg-3\" *ngFor=\"let currency of currencies\">\r\n            <div class=\"card\" (click)=\"route(currency)\" style=\" cursor: pointer; outline:none\">\r\n                <div class=\"card-body p-0 clearfix\">\r\n                    <i class=\"fa  bg-primary p-4 font-2xl mr-3 float-left\">\r\n                        <img style=\"height :10vh;width:5vw\" [src]=\"currency.cursymbolurl\" />\r\n                    </i>\r\n                    <div class=\"h5 text-primary mb-0 pt-3\">{{currency.currencyshortname}}</div>\r\n                    <div class=\"text-muted text-uppercase font-weight-bold font-xs\">{{currency.currencyname}}</div>\r\n                    Primary:\r\n                    <input type=\"checkbox\" (click)=\"primaryChange(currency, $event)\">\r\n\r\n\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n\r\n    </div>\r\n</div> -->"

/***/ }),

/***/ "../../../../../src/app/views/admin/currency.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CurrencyComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services__ = __webpack_require__("../../../../../src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CurrencyComponent = /** @class */ (function () {
    function CurrencyComponent(router, adminApi) {
        this.router = router;
        this.adminApi = adminApi;
        this.edit = false;
        this.user = {};
        this.currencyHeaderSettings = [{
                "visible": false,
                "fieldName": "currencyid",
                "displayName": "CurrencyId",
                "type": "Id"
            }, {
                "visible": true,
                "fieldName": "currencyname",
                "displayName": "Name",
                "type": "CurrencyName"
            }, {
                "visible": true,
                "fieldName": "currencyshortname",
                "displayName": "Symbol",
                "type": "String"
            }, {
                "visible": false,
                "fieldName": "cursymbolurl",
                "displayName": "cursymbolurl",
                "type": "none"
            }, {
                "visible": false,
                "fieldName": "curapiurl",
                "displayName": "curapiurl",
                "type": "none"
            }, {
                "visible": true,
                "fieldName": "bidperct",
                "displayName": "BID %",
                "type": "percent",
                "edit": true
            }, {
                "visible": true,
                "fieldName": "askperct",
                "displayName": "ASK %",
                "type": "percent",
                "edit": true
            }, {
                "visible": true,
                "fieldName": "canprimary",
                "displayName": "Primary",
                "type": "primary",
                "edit": true
            }, {
                "visible": true,
                "fieldName": "createdate",
                "displayName": "Create DateTime",
                "type": "dateTime"
            }, {
                "visible": true,
                "fieldName": "datetimeofupdate",
                "displayName": "Modified DateTime",
                "type": "dateTime"
            }];
    }
    CurrencyComponent.prototype.onChange = function (event) {
        console.log(event);
        console.log(this.currencies);
    };
    CurrencyComponent.prototype.ngOnInit = function () {
        this.getCurrencies();
        this.keys = this.currencyHeaderSettings;
    };
    CurrencyComponent.prototype.getCurrencies = function () {
        var _this = this;
        this.adminApi.getCurrencyList().subscribe(function (data) {
            _this.currencies = data;
        }, function (err) { return console.log(err); }, function () { return console.log("completed"); });
    };
    CurrencyComponent.prototype.primaryChange = function (currency, event) {
        var checked = event.target.checked;
        this.adminApi.setCurrencyPrimary(currency.currencyid)
            .subscribe(function (data) {
            console.log(data);
        }, function (error) { return console.log(error); });
    };
    CurrencyComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'currency',
            template: __webpack_require__("../../../../../src/app/views/admin/currency.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_2_app_services__["a" /* AdminService */]])
    ], CurrencyComponent);
    return CurrencyComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/admin/generic-wallet.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Generic Wallet</h2>\r\n<div>\r\n    <button class=\"btn btn-success btn-xs\"  data-toggle=\"modal\" (click)=\"myModal.show()\"  ><span class=\"glyphicon glyphicon-plus\"></span> Create Wallet </button>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<div bsModal #myModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h2 style=\"text-align:center!important; width:100%; \" >Create Wallet</h2>\r\n                <button type=\"button\" class=\"close\" (click)=\"myModal.hide()\" aria-label=\"Close\">\r\n                <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\" style=\"padding:2vw;\" >\r\n                <form name=\"form\" (ngSubmit)=\"f.form.valid && save(myModal)\" #f=\"ngForm\" novalidate>\r\n                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !accountname.valid }\">\r\n                        <label for=\"accountname\" >Account Name</label>\r\n                        <input type=\"text\" class=\"form-control\" name=\"accountname\" [(ngModel)]=\"user.accountname\" #accountname=\"ngModel\" placeholder=\"Account Name\" required />\r\n                        <div *ngIf=\"f.submitted && !accountname.valid\" class=\"help-block\">Account Id is required</div>\r\n                    </div>\r\n                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !email.valid }\">\r\n                        <label for=\"email\" >Email</label>\r\n                        <input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"Email Id\" [(ngModel)]=\"user.email\" #email=\"ngModel\" required />\r\n                        <div *ngIf=\"f.submitted && !email.valid\" class=\"help-block\">Email is required</div>\r\n                    </div>\r\n                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !currency.valid }\">\r\n                        <label for=\"mobile\" >Currency</label>\r\n                        <input type=\"tel\" class=\"form-control\" name=\"currency\" placeholder=\"Currency\" [(ngModel)]=\"user.currency\" #currency=\"ngModel\" required />\r\n                        <div *ngIf=\"f.submitted && !currency.valid\" class=\"help-block\">Select the Currency</div>\r\n                    </div>\r\n                    <div *ngIf=\"error\" class=\"alert alert-danger\">{{error}}</div>\r\n                    <button type=\"button\" style=\"float:right; margin-left:0.6vw;\"  class=\"btn btn-secondary\" (click)=\"myModal.hide()\">Close</button>\r\n                    <button class=\"center-block\" style=\"float:right; margin-left:0.6vw;\"  type=\"submit\" class=\"btn btn-primary\">\r\n                        <span style=\"margin-left:0.6vw;\">Create</span>\r\n                    </button>\r\n                </form>\r\n            </div>\r\n        </div><!-- /.modal-content -->\r\n    </div><!-- /.modal-dialog -->\r\n</div>\r\n    "

/***/ }),

/***/ "../../../../../src/app/views/admin/generic-wallet.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GenericWallet; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services__ = __webpack_require__("../../../../../src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var GenericWallet = /** @class */ (function () {
    function GenericWallet(router, admin) {
        this.router = router;
        this.admin = admin;
        this.user = {};
    }
    GenericWallet.prototype.ngOnInit = function () {
        this.admin.getWalletList().subscribe(function (data) { return console.log(data); });
    };
    GenericWallet.prototype.createGenericWallet = function () {
        var data = {
            "AccountID": 10,
            "CurrencyId": 4,
            "WalletAddress": "749easdadad",
            "PrivateKey": "AasdadS",
            "PublicKey": "ADAasdasdSDA",
            "Mobile": "801534090",
            "Label": "asdasd",
            "Email": "jp@gmail.com",
            "Guid": "zdasdsad"
        };
    };
    GenericWallet = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'generic-wallet',
            template: __webpack_require__("../../../../../src/app/views/admin/generic-wallet.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_2_app_services__["a" /* AdminService */]])
    ], GenericWallet);
    return GenericWallet;
}());



/***/ }),

/***/ "../../../../../src/app/views/admin/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__user_list_component__ = __webpack_require__("../../../../../src/app/views/admin/user-list.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_0__user_list_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__admin_manage_component__ = __webpack_require__("../../../../../src/app/views/admin/admin-manage.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__admin_manage_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__currency_component__ = __webpack_require__("../../../../../src/app/views/admin/currency.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__currency_component__["a"]; });





/***/ }),

/***/ "../../../../../src/app/views/admin/manage/admin-wallet.component.html":
/***/ (function(module, exports) {

module.exports = "<div style=\"margin-bottom:30px\">\r\n    <span *ngIf=\"userInfo\" style=\"text-transform:uppercase\">{{ userInfo.acname}}</span>'s Wallet\r\n    <button class=\"btn btn-success btn-xs\" data-toggle=\"modal\" (click)=\"myModal.show()\" style=\"float:right;\"> Create Wallet</button>\r\n</div>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-12\">\r\n\r\n        <!-- \r\n    <table class=\"table table-striped  table-hover \">\r\n        <thead>\r\n            <tr>\r\n                <th *ngFor=\"let key of keys\" class=\"col-header th-header\">\r\n                    <span>{{ key }}</span>\r\n                    <i class=\"fa\" [ngClass]=\"{'fa-sort': column != key, 'fa-sort-asc': (column == key && isDesc), 'fa-sort-desc': (column == key && !isDesc) }\"\r\n                        aria-hidden=\"true\"> </i>\r\n                </th>\r\n            </tr>\r\n        </thead>\r\n        <tbody>\r\n            <tr *ngFor=\"let item of currencies |  orderBy: {property: column, direction: direction}\">\r\n                <td *ngFor=\"let key of keys\" (click)=\"myModal.show(); selectedCurrency(item)\" [ngStyle]=\"{'text-align':isNumber(item[key])?'center':'left'}\">{{ item[key] }}</td>\r\n            </tr>\r\n        </tbody>\r\n    </table>\r\n    <div class=\"btn-toolbar\" role=\"toolbar\" style=\"margin: 0;\">\r\n        <div class=\"btn-group pull-right\">\r\n            <ul class=\"pagination\">\r\n                <li [ngClass]=\"{'disabled': (currentIndex == 1 || pageNumber == 0)}\">\r\n                    <a (click)=\"prevPage()\">Prev</a>\r\n                </li>\r\n                <li *ngFor=\"let page of pagesIndex\" [ngClass]=\"{'active': (currentIndex == page)}\">\r\n                    <a (click)=\"setPage(page)\">{{page}}</a>\r\n                </li>\r\n                <li [ngClass]=\"{'disabled': (currentIndex == pageNumber || pageNumber == 0)}\">\r\n                    <a (click)=\"nextPage()\">Next</a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</div> -->\r\n        <div class=\"panel\">\r\n            <div class=\"panel-heading\">\r\n                <div class=\"panel-action\">\r\n                    <form class=\"form-inline form-search\">\r\n                        <span class=\"form-group row\" style=\"float:right\">\r\n                            <label>Filter:</label>\r\n                            <input type=\"text\" class=\"form-control\">\r\n                        </span>\r\n                    </form>\r\n                </div>\r\n            </div>\r\n            <table class=\"table table-striped table-hover \">\r\n                <thead>\r\n                    <tr>\r\n                        <th class=\" th-header\">\r\n                            <span class=\"glyphicon glyphicon-plus\"></span>\r\n                        </th>\r\n                        <th *ngFor=\"let item of keys\" [hidden]=\"!item.visible\" class=\"th-header\" (click)=\"sort(item)\">\r\n                            <span>{{ item.displayName }} </span>&nbsp;\r\n                            <i class=\"fa\" [ngClass]=\"{'fa-sort': column != item.fieldName, 'fa-sort-asc': (column == item.fieldName && isDesc), 'fa-sort-desc': (column == item.fieldName && !isDesc) }\"\r\n                                aria-hidden=\"true\"> </i>\r\n                        </th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody>\r\n                    <tr *ngFor=\"let wallet of userWallets\">\r\n                        <td [ngStyle]=\"{'text-align':'center'}\">\r\n                            <a class=\"btn btn-primary btn-sm\" (click)=\"balanceModal.show();balance.PrimaryWallet=wallet.walletguid\">\r\n                                <span class=\"glyphicon glyphicon-plus\" style=\"color:white\"></span>\r\n                            </a>\r\n                            <a class=\"btn btn-primary btn-sm\">\r\n                                <span class=\"glyphicon glyphicon-minus\" style=\"color:white\"></span>\r\n                            </a>\r\n                            <a class=\"btn btn-primary btn-sm\" (click)=\"viewBalance.show();getBalance(wallet.walletguid)\">\r\n                                <span class=\"fa fa-dollar\" style=\"color:white\"></span>\r\n                            </a>\r\n                        </td>\r\n                        <td *ngFor=\"let item of keys\" [hidden]=\"!item.visible\" [ngStyle]=\"{'text-align':'center'}\">\r\n                            {{ wallet[ item.fieldName] }}\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n<div bsModal #myModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h2 style=\"text-align:center!important; width:100%; \">Create Wallet</h2>\r\n                <button type=\"button\" class=\"close\" (click)=\"myModal.hide()\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\" style=\"padding:2vw;\">\r\n                <form name=\"form-inline\" (ngSubmit)=\"f.form.valid && save(f)\" #f=\"ngForm\" novalidate>\r\n                    <div *ngIf=\"errorResponse\" style=\"text-align:center;color:red\">{{errorMessage}}</div>\r\n                    <!-- <div class=\"form-group row\">\r\n                        <label for=\"AccountID\" class=\"col-sm-3 col-form-label\">Account Id</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" name=\"AccountID\" disabled [(ngModel)]=\"user.AccountID\" #AccountID=\"ngModel\" placeholder=\"Account ID\"\r\n                            />\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    <div class=\"form-group row\">\r\n                        <label for=\"CurrencyId\" class=\"col-sm-3 col-form-label\">Currency</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <select class=\"form-control\" [(ngModel)]=\"user.CurrencyId\" name=\"CurrencyId\">\r\n                                <option *ngFor=\"let currecy of filteredCurrencies\" [value]=\"currecy.currencyid\">{{currecy.currencyname}}</option>\r\n                            </select>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"form-group row\" [ngClass]=\"{ 'has-error': WalletAddress.touched && !WalletAddress.valid }\">\r\n                        <label for=\"WalletAddress\" class=\"col-sm-3 col-form-label\">WalletAddress</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" name=\"PrivateKey\" placeholder=\"Enter WalletAddress\" [(ngModel)]=\"user.WalletAddress\"\r\n                                #WalletAddress=\"ngModel\" required />\r\n                            <div *ngIf=\"WalletAddress.touched && !WalletAddress.valid\" class=\"help-block\">WalletAddress is required</div>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"form-group row\" [ngClass]=\"{ 'has-error': PrivateKey.touched && !PrivateKey.valid }\">\r\n                        <label for=\"PrivateKey\" class=\"col-sm-3 col-form-label\">Private Key</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" name=\"PrivateKey\" placeholder=\"Enter Private Key\" [(ngModel)]=\"user.PrivateKey\" #PrivateKey=\"ngModel\"\r\n                                required />\r\n                            <div *ngIf=\"PrivateKey.touched && !PrivateKey.valid\" class=\"help-block\">Private Key is required</div>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"form-group row\" [ngClass]=\"{ 'has-error': PublicKey.touched  && !PublicKey.valid }\">\r\n                        <label for=\"PublicKey\" class=\"col-sm-3 col-form-label\">Public Key</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"email\" class=\"form-control\" name=\"PublicKey\" placeholder=\"Enter Public Key\" [(ngModel)]=\"user.PublicKey\" #PublicKey=\"ngModel\"\r\n                                required />\r\n                            <div *ngIf=\"PublicKey.touched && !PublicKey.valid\" class=\"help-block\">Public Key is required</div>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"form-group row\" [ngClass]=\"{ 'has-error': Mobile.touched && !Mobile.valid }\">\r\n                        <label for=\"Mobile\" class=\"col-sm-3 col-form-label\">Mobile</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"tel\" class=\"form-control\" name=\"Mobile\" placeholder=\"Enter Mobile\" [(ngModel)]=\"user.Mobile\" #Mobile=\"ngModel\"\r\n                                required />\r\n                            <div *ngIf=\"Mobile.touched && !Mobile.valid\" class=\"help-block\">Mobile is required</div>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group row\" [ngClass]=\"{ 'has-error': Label.touched && !Label.valid }\">\r\n                        <label for=\"Label\" class=\"col-sm-3 col-form-label\">Label</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" name=\"Label\" placeholder=\"Enter Label\" [(ngModel)]=\"user.Label\" #Label=\"ngModel\"\r\n                                required />\r\n                            <div *ngIf=\"Label.touched && !Label.valid\" class=\"help-block\">Label  is required</div>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"form-group row\" [ngClass]=\"{ 'has-error': Email.touched && !Email.valid }\">\r\n                        <label for=\"Email\" class=\"col-sm-3 col-form-label\">Email</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"email\" class=\"form-control\" name=\"Email\" placeholder=\"Enter Email\" [(ngModel)]=\"user.Email\" #Email=\"ngModel\"\r\n                                required />\r\n                            <div *ngIf=\"Email.touched && !Email.valid\" class=\"help-block\">Email is required</div>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"form-group row\" [ngClass]=\"{ 'has-error': Guid.touched && !Guid.valid }\">\r\n                        <label for=\"Guid\" class=\"col-sm-3 col-form-label\">Guid</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" name=\"Guid\" placeholder=\"Enter Guid\" [(ngModel)]=\"user.Guid\" #Guid=\"ngModel\" required\r\n                            />\r\n                            <div *ngIf=\"Guid.touched && !Guid.valid\" class=\"help-block\">Guid is required</div>\r\n                        </div>\r\n                    </div>\r\n                    <div *ngIf=\"error\" class=\"alert alert-danger\">{{error}}</div>\r\n                    <button type=\"button\" style=\"float:right; margin-left:0.6vw;\" class=\"btn btn-secondary\" (click)=\"myModal.hide()\">Close</button>\r\n                    <button class=\"center-block\" style=\"float:right; margin-left:0.6vw;\" type=\"submit\" class=\"btn btn-primary\">\r\n                        <span style=\"margin-left:0.6vw;\">Create</span>\r\n                    </button>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n<div bsModal #balanceModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"Add Balance\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h2 style=\"text-align:center!important; width:100%; \">Add Balance</h2>\r\n                <button type=\"button\" class=\"close\" (click)=\"balanceModal.hide()\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\" style=\"padding:2vw;\">\r\n                <form name=\"form-inline\" (ngSubmit)=\"addBalaceForm.form.valid && addBalance(balanceModal,addBalaceForm) \" #addBalaceForm=\"ngForm\"\r\n                    novalidate>\r\n                    <!-- <div class=\"form-group row\">\r\n                        <label for=\"primaryWallet\" class=\"col-sm-3 col-form-label\">Primary Wallet</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" disabled name=\"primaryWallet\" [(ngModel)]=\"balance.PrimaryWallet\" #primaryWallet=\"ngModel\"\r\n                                placeholder=\"Enter Primary Wallet\" />\r\n                        </div>\r\n                    </div> -->\r\n\r\n                    <div class=\"form-group row\">\r\n                        <label for=\"Units\" class=\"col-sm-3 col-form-label\">Units</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" name=\"Units\" [(ngModel)]=\"balance.Units\" #Units=\"ngModel\" placeholder=\"Enter Units\"\r\n                            />\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group row\">\r\n                        <label for=\"usdrate\" class=\"col-sm-3 col-form-label\">USD Rate</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" name=\"usdrate\" [(ngModel)]=\"balance.usdrate\" #usdrate=\"ngModel\" placeholder=\"Enter rate\"\r\n                            />\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group row\">\r\n                        <label for=\"usdAmount\" class=\"col-sm-3 col-form-label\">USD Amount</label>\r\n                        <div class=\"col-sm-9\">\r\n                            <input type=\"text\" class=\"form-control\" name=\"usdAmount\" [(ngModel)]=\"balance.usdAmount\" #usdAmount=\"ngModel\" placeholder=\"Enter USD Amount\"\r\n                            />\r\n                        </div>\r\n                    </div>\r\n\r\n                    <button type=\"button\" style=\"float:right; margin-left:0.6vw;\" class=\"btn btn-secondary\" (click)=\"balanceModal.hide()\">Close</button>\r\n                    <button class=\"center-block\" style=\"float:right; margin-left:0.6vw;\" type=\"submit\" class=\"btn btn-primary\">\r\n                        <span style=\"margin-left:0.6vw;\">Add</span>\r\n                    </button>\r\n\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n<div bsModal #viewBalance=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h4 class=\"modal-title\">User Balance</h4>\r\n                <button type=\"button\" class=\"close\" (click)=\"viewBalance.hide();userWalletBalance=''\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <p> Balance &hellip;{{userWalletBalance | number : '0.0-7'}}\r\n                </p>\r\n            </div>\r\n        </div>\r\n        <!-- /.modal-content -->\r\n    </div>\r\n    <!-- /.modal-dialog -->\r\n</div>\r\n<!-- /.modal -->"

/***/ }),

/***/ "../../../../../src/app/views/admin/manage/admin-wallet.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminWalletComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services__ = __webpack_require__("../../../../../src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AdminWalletComponent = /** @class */ (function () {
    function AdminWalletComponent(router, adminApi, activatedRoute) {
        this.router = router;
        this.adminApi = adminApi;
        this.activatedRoute = activatedRoute;
        this.user = {};
        this.headerSettings = [{
                "visible": false,
                "fieldName": "walletacid",
                "displayName": "WalletAccountId"
            }, {
                "visible": false,
                "fieldName": "accountid",
                "displayName": "AccountId"
            }, {
                "visible": false,
                "fieldName": "currencyid",
                "displayName": "CurrencyId",
            }, {
                "visible": true,
                "fieldName": "currencyname",
                "displayName": "CurrencyName"
            }, {
                "visible": true,
                "fieldName": "walletaddress",
                "displayName": "WalletAddress"
            }, {
                "visible": true,
                "fieldName": "publickey",
                "displayName": "PublicKey"
            },
            {
                "visible": true,
                "fieldName": "email",
                "displayName": "Email"
            }, {
                "visible": true,
                "fieldName": "fundslocked",
                "displayName": "FundsLocked"
            }, {
                "visible": true,
                "fieldName": "walletguid",
                "displayName": "WalletGuid"
            }, {
                "visible": false,
                "fieldName": "cursymbolurl",
                "displayName": "CurrencySymbolURL"
            }];
        this.balance = {};
    }
    AdminWalletComponent.prototype.ngOnDestroy = function () {
        if (this.queryParamSubscriber$)
            this.queryParamSubscriber$.unsubscribe();
    };
    AdminWalletComponent.prototype.ngOnInit = function () {
        var _this = this;
        //    this.getCurrencies();
        this.keys = this.headerSettings;
        this.queryParamSubscriber$ = this.activatedRoute.queryParams.subscribe(function (params) {
            _this.accountId = params['accountId'] || null;
            _this.user.AccountID = _this.accountId;
            _this.getWallets();
            _this.getAccountInfo();
        });
    };
    AdminWalletComponent.prototype.init = function () {
        var wallets = this.userWallets;
        this.filteredCurrencies = [];
        for (var currencyIndex in this.currencies) {
            var flag = true;
            var currency = this.currencies[currencyIndex];
            for (var index in wallets) {
                var wallet = wallets[index];
                if (wallet.currencyid == currency.currencyid) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                this.filteredCurrencies.push(currency);
            }
        }
    };
    AdminWalletComponent.prototype.getCurrencies = function () {
        var _this = this;
        this.adminApi.getCurrencyList().subscribe(function (data) {
            if (Array.isArray(data)) {
                _this.currencies = data;
                _this.init();
            }
        });
    };
    AdminWalletComponent.prototype.getWallets = function () {
        var _this = this;
        this.adminApi.getWalletsPerUser(this.accountId).subscribe(function (data) {
            if (Array.isArray(data)) {
                _this.userWallets = data;
                _this.getCurrencies();
            }
        }, function (err) { return console.log(err); }, function () { return console.log("completed"); });
    };
    AdminWalletComponent.prototype.getAccountInfo = function () {
        var _this = this;
        this.adminApi.getUsersByAccountId(this.accountId).subscribe(function (data) {
            if (Array.isArray(data)) {
                _this.userInfo = data[0];
            }
        }, function (err) { return console.log(err); }, function () { return console.log("completed"); });
    };
    AdminWalletComponent.prototype.save = function (createWalletForm) {
        var _this = this;
        var data = this.user;
        this.adminApi.createGenericWallet(data)
            .subscribe(function (data) {
            createWalletForm.resetForm();
            _this.errorResponse = true;
            _this.errorMessage = "Wallet Created";
            _this.getWallets();
            setTimeout(function () { return _this.user.AccountID = _this.accountId; }, 0);
        }, function (err) { return console.log(data); });
        this.init();
    };
    AdminWalletComponent.prototype.addBalance = function (balanceModal, addBalaceForm) {
        var data = this.balance;
        this.adminApi.addBalance(data)
            .subscribe(function (data) {
            addBalaceForm.resetForm();
            setTimeout(function () {
                balanceModal.hide();
            }, 0);
        });
    };
    AdminWalletComponent.prototype.isNumber = function (val) { return typeof val === 'number'; };
    AdminWalletComponent.prototype.selectedCurrency = function (currency) {
        this.user.CurrencyId = currency['currencyid'];
    };
    AdminWalletComponent.prototype.getBalance = function (walletguid) {
        var _this = this;
        this.adminApi.getWalletBalance(walletguid).subscribe(function (data) {
            _this.userWalletBalance = data;
        });
    };
    AdminWalletComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'admin-wallet',
            template: __webpack_require__("../../../../../src/app/views/admin/manage/admin-wallet.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_2_app_services__["a" /* AdminService */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */]])
    ], AdminWalletComponent);
    return AdminWalletComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/admin/manage/bid-ask.component.html":
/***/ (function(module, exports) {

module.exports = "\r\n<div class=\"row\">\r\n    <div class=\"col-md-6\">\r\n            <table-layout [records]=buyHistory [settings]=\"buySellheaderSettings\" title=\"BIDS\"></table-layout>\r\n\r\n    </div>\r\n    <div class=\"col-md-6\">\r\n            <table-layout [records]=askHistory [settings]=\"buySellheaderSettings\" title=\"ASKS\"></table-layout>\r\n\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/admin/manage/bid-ask.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BidAskComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services__ = __webpack_require__("../../../../../src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BidAskComponent = /** @class */ (function () {
    function BidAskComponent(router, activatedRoute, admin) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.admin = admin;
        this.buySellheaderSettings = [{
                "visible": false,
                "fieldName": "secondcurid",
                "displayName": "secondcurid"
            }, {
                "visible": true,
                "fieldName": "units",
                "displayName": "Units"
            }, {
                "visible": true,
                "fieldName": "Total",
                "displayName": "Total"
            }, {
                "visible": true,
                "fieldName": "rate",
                "displayName": "Rate"
            }, {
                "visible": true,
                "fieldName": "sum",
                "displayName": "SUM"
            }];
    }
    BidAskComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.queryParamSubscriber$ = this.activatedRoute.queryParams.subscribe(function (params) {
            _this.accountId = params['accountId'] || null;
            _this.getUserAsks(_this.accountId);
            _this.getUserBids(_this.accountId);
        });
    };
    BidAskComponent.prototype.ngOnDestroy = function () {
        if (this.queryParamSubscriber$)
            this.queryParamSubscriber$.unsubscribe();
    };
    BidAskComponent.prototype.getUserBids = function (accountId) {
        var _this = this;
        this.admin.getUserBuyHistory(accountId).subscribe(function (res) {
            if (Array.isArray(res))
                _this.buyHistory = res;
        });
    };
    BidAskComponent.prototype.getUserAsks = function (accountId) {
        var _this = this;
        this.admin.getUserSellHistory(accountId).subscribe(function (res) {
            if (Array.isArray(res))
                _this.askHistory = res;
        });
    };
    BidAskComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'bid-ask',
            template: __webpack_require__("../../../../../src/app/views/admin/manage/bid-ask.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_2_app_services__["a" /* AdminService */]])
    ], BidAskComponent);
    return BidAskComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/admin/manage/index.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__admin_wallet_component__ = __webpack_require__("../../../../../src/app/views/admin/manage/admin-wallet.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__admin_wallet_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__reset_pwd_component__ = __webpack_require__("../../../../../src/app/views/admin/manage/reset-pwd.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_1__reset_pwd_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__bid_ask_component__ = __webpack_require__("../../../../../src/app/views/admin/manage/bid-ask.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__bid_ask_component__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__trans_history_component__ = __webpack_require__("../../../../../src/app/views/admin/manage/trans-history.component.ts");
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_3__trans_history_component__["a"]; });






/***/ }),

/***/ "../../../../../src/app/views/admin/manage/reset-pwd.component.html":
/***/ (function(module, exports) {

module.exports = "<h2>Reset Password</h2>\r\n\r\n<form name=\"form\" (ngSubmit)=\"f.form.valid && save()\" #f=\"ngForm\" novalidate>\r\n    <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !password.valid }\">\r\n        <label for=\"password\">New Password</label>\r\n        <input type=\"password\" class=\"form-control\" name=\"password\" placeholder=\"Password\" [(ngModel)]=\"model.password\" #password=\"ngModel\" required />\r\n        <div *ngIf=\"f.submitted && !password.valid\" class=\"help-block\">Password is required</div>\r\n    </div>\r\n    <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !confirmPassword.valid }\">\r\n        <label for=\"confirmPassword\">Confirm Password</label>\r\n        <input type=\"password\" class=\"form-control\" name=\"confirmPassword\" placeholder=\"Confirm Password\" [(ngModel)]=\"model.confirmPassword\" #confirmPassword=\"ngModel\" required />\r\n        <div *ngIf=\"f.submitted && !confirmPassword.valid\" class=\"help-block\">Password is required</div>\r\n    </div>\r\n    <div *ngIf=\"error\" class=\"alert alert-danger\">{{error}}</div>\r\n    <button class=\"center-block\" style=\"float:right; margin-left:0.6vw;\"  type=\"submit\" class=\"btn btn-primary\">\r\n        <span style=\"margin-left:0.6vw;\" (submit)=\"save();\" >Change</span>\r\n    </button>\r\n</form>"

/***/ }),

/***/ "../../../../../src/app/views/admin/manage/reset-pwd.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ResetPasswordComponent = /** @class */ (function () {
    function ResetPasswordComponent(router) {
        this.router = router;
        this.model = {};
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
    };
    ResetPasswordComponent.prototype.save = function () {
        this.error = null;
        if (this.model.password === this.model.confirmPassword) {
            console.log("Changed Successfully");
        }
        else {
            this.error = "New Password and Confirm Password should be Same";
        }
    };
    ResetPasswordComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'reset-pwd',
            template: __webpack_require__("../../../../../src/app/views/admin/manage/reset-pwd.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/admin/manage/trans-history.component.html":
/***/ (function(module, exports) {

module.exports = "    <app-table [records]=transactionHistory title=\"TRANSACTION HISTORY\"></app-table>\r\n"

/***/ }),

/***/ "../../../../../src/app/views/admin/manage/trans-history.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TransHistoryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services__ = __webpack_require__("../../../../../src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TransHistoryComponent = /** @class */ (function () {
    function TransHistoryComponent(router, activatedRoute, admin) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.admin = admin;
        this.buySellheaderSettings = [{
                "visible": false,
                "fieldName": "secondcurid",
                "displayName": "secondcurid"
            }, {
                "visible": true,
                "fieldName": "units",
                "displayName": "Units"
            }, {
                "visible": true,
                "fieldName": "Total",
                "displayName": "Total"
            }, {
                "visible": true,
                "fieldName": "rate",
                "displayName": "Rate"
            }, {
                "visible": true,
                "fieldName": "sum",
                "displayName": "SUM"
            }];
    }
    TransHistoryComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.queryParamSubscriber$ = this.activatedRoute.queryParams.subscribe(function (params) {
            _this.accountId = params['accountId'] || null;
            _this.getUserTransactionHistory(_this.accountId);
        });
    };
    TransHistoryComponent.prototype.ngOnDestroy = function () {
        if (this.queryParamSubscriber$)
            this.queryParamSubscriber$.unsubscribe();
    };
    TransHistoryComponent.prototype.getUserTransactionHistory = function (accountId) {
        var _this = this;
        this.admin.getUserTransactionHistory(accountId).subscribe(function (res) {
            if (Array.isArray(res))
                _this.transactionHistory = res;
        });
    };
    TransHistoryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'trans-history',
            template: __webpack_require__("../../../../../src/app/views/admin/manage/trans-history.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* ActivatedRoute */], __WEBPACK_IMPORTED_MODULE_2_app_services__["a" /* AdminService */]])
    ], TransHistoryComponent);
    return TransHistoryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/admin/user-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <div style=\"margin-top:1vw;\">\r\n        <span class=\"subtitle\">USER</span>\r\n        <button style=\"float:right;\" class=\"btn btn-success btn-xs\" data-toggle=\"modal\" (click)=\"myModal.show()\">\r\n            <span class=\"glyphicon glyphicon-plus\"></span> Add New User </button>\r\n    </div>\r\n    <hr>\r\n\r\n    <div>\r\n        <div class=\"panel\">\r\n            <div class=\"panel-heading\">\r\n                <div class=\"panel-action\">\r\n                    <form class=\"form-inline form-search\">\r\n                        <span class=\"form-group row\" style=\"float:right\">\r\n                            <label>Filter:</label>\r\n                            <input type=\"text\" class=\"form-control\">\r\n                        </span>\r\n                    </form>\r\n                </div>\r\n            </div>\r\n            <table class=\"table table-striped table-hover \">\r\n                <thead>\r\n                    <tr>\r\n                        <th *ngFor=\"let item of keys\" [hidden]=\"!item.visible\" class=\"th-header\" (click)=\"sort(item)\">\r\n                            <span>{{ item.displayName }} </span>&nbsp;\r\n                            <i class=\"fa\" [ngClass]=\"{'fa-sort': column != item.fieldName, 'fa-sort-asc': (column == item.fieldName && isDesc), 'fa-sort-desc': (column == item.fieldName && !isDesc) }\"\r\n                                aria-hidden=\"true\"> </i>\r\n                        </th>\r\n                        <th class=\"col-header th-header\">ACTION</th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody *ngIf=\"users\">\r\n                    <tr *ngFor=\"let rowData of users |  orderBy: {property: column, direction: direction}\">\r\n                        <td *ngFor=\"let item of keys\" [hidden]=\"!item.visible\" [ngStyle]=\"{'text-align':'center'}\">\r\n                            {{ rowData[ item.fieldName] }}\r\n                        </td>\r\n                        <td [ngStyle]=\"{'text-align':'center'}\">\r\n                            <a class=\"btn btn-primary btn-sm\" routerLink=\"/home/admin/manage/wallet\" [queryParams]=\"{accountId: rowData.accountid }\">\r\n                                <span class=\"fa fa-btc\" style=\"color:white\"></span>\r\n                            </a>\r\n\r\n                            <a class=\"btn btn-primary btn-sm\" routerLink=\"/home/admin/manage/reset\" [queryParams]=\"{accountId: rowData.accountid}\">\r\n                                <span class=\"fa-passwd-reset fa-stack\" style=\"font-size: 0.85rem;\">\r\n                                    <i class=\"fa fa-undo fa-stack-2x\"></i>\r\n                                    <i class=\"fa fa-lock fa-stack-1x\"></i>\r\n                                </span>\r\n                            </a>\r\n                            <a class=\"btn btn-primary btn-sm\" routerLink=\"/home/admin/manage/trans\" [queryParams]=\"{accountId: rowData.accountid }\">\r\n                                <span class=\"fa fa-exchange\" style=\"color:white\"></span>\r\n                            </a>\r\n\r\n                            <a class=\"btn btn-primary btn-sm\" routerLink=\"/home/admin/manage/bidask\" [queryParams]=\"{accountId: rowData.accountid }\">\r\n                                <span class=\"fa fa-a\" style=\"color:white\"></span>\r\n                            </a>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n<div bsModal #myModal=\"bs-modal\" class=\"modal fade\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\r\n    <div class=\"modal-dialog\" role=\"document\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h2 style=\"text-align:center!important; width:100%; \">Create User</h2>\r\n                <button type=\"button\" class=\"close\" (click)=\"myModal.hide()\" aria-label=\"Close\">\r\n                    <span aria-hidden=\"true\">&times;</span>\r\n                </button>\r\n            </div>\r\n            <div class=\"modal-body\" style=\"padding:2vw;\">\r\n                <form name=\"form\" (ngSubmit)=\"signUpForm.form.valid && register(myModal,signUpForm) \" #signUpForm=\"ngForm\" novalidate>\r\n                    <div *ngIf=\"errorResponse\" style=\"text-align:center;color:red\">{{errorMessage}}</div>\r\n                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': accountname.touched && !accountname.valid }\">\r\n                        <label for=\"accountname\">Account Name</label>\r\n                        <input type=\"text\" class=\"form-control\" name=\"accountname\" [(ngModel)]=\"user.acname\" #accountname=\"ngModel\" placeholder=\"Account Name\"\r\n                            required />\r\n                        <div *ngIf=\"accountname.touched && !accountname.valid\" class=\"help-block\">Account Name is required</div>\r\n                    </div>\r\n                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': email.touched && !email.valid }\">\r\n                        <label for=\"email\">Email</label>\r\n                        <input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"Email Id\" [(ngModel)]=\"user.emailid\" #email=\"ngModel\"\r\n                            required pattern='^(([^<>()\\[\\]\\\\.,;:\\s@\"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$'\r\n                        />\r\n                        <div *ngIf=\"email.touched && !email.valid\" class=\"help-block\">Please enter valid Email</div>\r\n                    </div>\r\n                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': mobile.touched && !mobile.valid }\">\r\n                        <label for=\"mobile\">Mobile Number</label>\r\n                        <input type=\"tel\" class=\"form-control\" name=\"mobile\" placeholder=\"Mobile Number\" [(ngModel)]=\"user.mobile\" #mobile=\"ngModel\"\r\n                            required pattern='^(\\+91-|\\+91|0)?\\d{10}$' />\r\n                        <div *ngIf=\"mobile.touched && !mobile.valid\" class=\"help-block\">Please enter vaild Mobile Number</div>\r\n                    </div>\r\n                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': password.touched && !password.valid }\">\r\n                        <label for=\"password\">Password</label>\r\n                        <input type=\"password\" class=\"form-control\" name=\"password\" placeholder=\"Password\" [(ngModel)]=\"user.appuser.Password\" #password=\"ngModel\"\r\n                            required pattern='^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\\w\\s]).{6,}$' />\r\n                        <!-- <div *ngIf=\"password.touched &&!password.valid\" class=\"help-block\">Please enter valid Password</div> -->\r\n                        <div *ngIf=\"password.touched &&!password.valid\" class=\"help-block\">Password must contain one upper and lowercase, number and special character </div>\r\n                    </div>\r\n                    <div *ngIf=\"error\" class=\"alert alert-danger\">{{error}}</div>\r\n                    <button type=\"button\" style=\"float:right; margin-left:0.6vw;\" class=\"btn btn-secondary\" (click)=\"myModal.hide()\">Close</button>\r\n                    <button [disabled]=\"!signUpForm.valid\" class=\"center-block\" style=\"float:right; margin-left:0.6vw;\" type=\"submit\" class=\"btn btn-primary\">\r\n                        <span style=\"margin-left:0.6vw;\" (submit)=\"save();\">Save</span>\r\n                    </button>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/admin/user-list.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserListComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_app_services__ = __webpack_require__("../../../../../src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var UserListComponent = /** @class */ (function () {
    function UserListComponent(router, userService, authService) {
        this.router = router;
        this.userService = userService;
        this.authService = authService;
        this.user = { "acname": "", "emailid": "", "mobile": "", "appuser": { "email": "", "Password": "" } };
        this.isDesc = false;
        this.column = '';
        this.dataJSON = [{ "MarketName": "BTC-ZEN", "Currency": "Zencash", "Volume": "217.043", "Changes": "-11.0", "LastPrice": "0.00279140", "High": "0.00318628", "Low": "0.00269720", "Spread": "0.6%", "Added": "06/05/2017", "img": "bitcoin.png", "Status": "Biggest % Gain" }];
        this.headerSettings = [{
                "visible": false,
                "fieldName": "accountid",
                "displayName": "AccountId"
            }, {
                "visible": true,
                "fieldName": "acname",
                "displayName": "AccountName"
            }, {
                "visible": true,
                "fieldName": "email",
                "displayName": "Email",
            }, {
                "visible": true,
                "fieldName": "mobile",
                "displayName": "Mobile"
            }, {
                "visible": false,
                "fieldName": "userid",
                "displayName": "UserId"
            }, {
                "visible": true,
                "fieldName": "acdate",
                "displayName": "AccountDate"
            }];
    }
    UserListComponent.prototype.ngOnInit = function () {
        this.getUserList();
        this.keys = this.headerSettings;
    };
    UserListComponent.prototype.manage = function (data) {
        console.log("Manage data : " + data);
    };
    UserListComponent.prototype.register = function (myModal, signUpForm) {
        this.user.appuser.email = this.user.emailid;
        this.registerUser(this.user, myModal, signUpForm);
    };
    UserListComponent.prototype.getUserList = function () {
        var _this = this;
        this.userService.getUsers().subscribe(function (data) {
            if (Array.isArray(data))
                _this.users = data;
        }, function (err) { return console.log(err); });
    };
    UserListComponent.prototype.registerUser = function (user, myModal, signUpForm) {
        var _this = this;
        this.authService.registerUser(user).subscribe(function (res) {
            if (Array.isArray(res)) {
                var error = res[0];
                var code = error["code"];
                _this.errorResponse = true;
                _this.errorMessage = code;
                _this.user.emaild = "";
                _this.user.appuser.email = "";
            }
            else {
                _this.errorResponse = true;
                _this.errorMessage = "Account Created";
                _this.getUserList();
            }
            // myModal.hide()
            signUpForm.resetForm();
        }, function (err) { return console.log(err); });
    };
    UserListComponent.prototype.isNumber = function (val) { return typeof val === 'number'; };
    UserListComponent.prototype.sort = function (property) {
        this.isDesc = !this.isDesc; //change the direction    
        this.column = property;
        this.direction = this.isDesc ? 1 : -1;
    };
    ;
    UserListComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'user-list',
            template: __webpack_require__("../../../../../src/app/views/admin/user-list.component.html")
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_2_app_services__["e" /* UserService */], __WEBPACK_IMPORTED_MODULE_2_app_services__["a" /* AdminService */]])
    ], UserListComponent);
    return UserListComponent;
}());



/***/ })

});
//# sourceMappingURL=admin.module.chunk.js.map