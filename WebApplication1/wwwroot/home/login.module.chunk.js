webpackJsonp(["login.module"],{

/***/ "../../../../../src/app/views/login/login-routing.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginRoutingModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_component__ = __webpack_require__("../../../../../src/app/views/login/login.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__login_component__["a" /* LoginComponent */], data: { title: 'Login' } },
];
var LoginRoutingModule = /** @class */ (function () {
    function LoginRoutingModule() {
    }
    LoginRoutingModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */].forChild(routes)],
            exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* RouterModule */]]
        })
    ], LoginRoutingModule);
    return LoginRoutingModule;
}());



/***/ }),

/***/ "../../../../../src/app/views/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <!-- <div style=\"padding:3vw;\">\r\n        <img src=\"assets/img/logo/btr-logo.png\" class=\"img-responsive center-block\" id=\"logo-image\">\r\n    </div> -->\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-4\"></div>\r\n        <div class=\"col-lg-4\">\r\n            <div class=\"card pad-md shadow-round\" id=\"login\">\r\n                <h2 style=\"text-align:center\">LOG IN</h2>\r\n                <form name=\"form\" class=\"form-horizontal new-lg-form\" (ngSubmit)=\"f.form.valid  && login(myModal) \" #f=\"ngForm\" novalidate>\r\n                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !username.valid }\">\r\n                        <input type=\"email\" class=\"form-control\" name=\"username\" [(ngModel)]=\"model.username\" #username=\"ngModel\" placeholder=\"Email Address\"\r\n                            required />\r\n                        <div *ngIf=\"f.submitted && !username.valid\" class=\"help-block\">Username is required</div>\r\n                    </div>\r\n                    <div class=\"form-group\" [ngClass]=\"{ 'has-error': f.submitted && !password.valid }\">\r\n                        <input type=\"password\" class=\"form-control\" name=\"password\" placeholder=\"Password\" [(ngModel)]=\"model.password\" #password=\"ngModel\"\r\n                            required />\r\n                        <div *ngIf=\"f.submitted && !password.valid\" class=\"help-block\">Password is required</div>\r\n                    </div>\r\n                    <div *ngIf=\"error\" class=\"alert alert-danger\">{{error}}</div>\r\n                    <div class=\"form-group\">\r\n                        <div class=\"col-md-12\">\r\n                            <div class=\"checkbox checkbox-info pull-left p-t-0\">\r\n                                <input id=\"checkbox-signup\" type=\"checkbox\">\r\n                                <label for=\"checkbox-signup\"> Remember me </label>\r\n                            </div>\r\n                            <a href=\"javascript:void(0)\" id=\"to-recover\" class=\"text-dark pull-right\">\r\n                                <i class=\"fa fa-lock m-r-5\"></i> Forgot pwd?</a>\r\n                        </div>\r\n                    </div>\r\n                    <div class=\"form-group\">\r\n                        <re-captcha [(ngModel)]=\"formModel.captcha\" name=\"captcha\" required #captchaControl=\"ngModel\"></re-captcha>\r\n                        <div [hidden]=\"captchaControl.valid || captchaControl.pristine\" class=\"error\">Captcha must be solved</div>\r\n                        <div [hidden]=\"!captchaControl.valid\" class=\"success\">Captcha is valid</div>\r\n                    </div>\r\n                    <hr/>\r\n                    <div class=\"form-group text-center m-t-20\">\r\n                        <div class=\"col-xs-12\">\r\n                            <button [disabled]=\"!f.valid\" class=\"btn btn-info btn-lg btn-block btn-rounded text-uppercase waves-effect waves-light\" type=\"submit\">Log In</button>\r\n                        </div>\r\n                    </div>\r\n\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <h5 style=\"text-align: center\">Don't have an account?\r\n        <a routerLink=\"/signup\">Sign Up</a>\r\n    </h5>\r\n</div>"

/***/ }),

/***/ "../../../../../src/app/views/login/login.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__("../../../router/esm5/router.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_index__ = __webpack_require__("../../../../../src/app/services/index.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = /** @class */ (function () {
    function LoginComponent(router, userService, authenticationService) {
        this.router = router;
        this.userService = userService;
        this.authenticationService = authenticationService;
        this.formModel = {};
        this.model = {};
        this.error = '';
        this.isUser = false;
        this.isAdmin = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent.prototype.login = function (myModal) {
        var _this = this;
        this.error = null;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(function (result) {
            if (result === true) {
                _this.isLogin = true;
                // myModal.hide(); // close the modal
                _this.router.navigate(['home/markets']);
                //get user role
                _this.authenticationService.getUserRole();
                if (!_this.rememberMe) {
                    // this.model = {}
                }
                _this.formModel.captcha = '';
            }
            else {
                _this.error = 'Username or password is incorrect';
                _this.isLogin = false;
                _this.formModel.captcha = '';
            }
        });
    };
    LoginComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'login',
            template: __webpack_require__("../../../../../src/app/views/login/login.component.html"),
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */], __WEBPACK_IMPORTED_MODULE_2__services_index__["e" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__services_index__["d" /* AuthenticationService */]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "../../../../../src/app/views/login/login.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginModule", function() { return LoginModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__("../../../forms/esm5/forms.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_component__ = __webpack_require__("../../../../../src/app/views/login/login.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_routing_module__ = __webpack_require__("../../../../../src/app/views/login/login-routing.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_app_shared_shared_module__ = __webpack_require__("../../../../../src/app/shared/shared.module.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var LoginModule = /** @class */ (function () {
    function LoginModule() {
    }
    LoginModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_4__login_routing_module__["a" /* LoginRoutingModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_forms__["FormsModule"],
                __WEBPACK_IMPORTED_MODULE_5_app_shared_shared_module__["a" /* SharedModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_3__login_component__["a" /* LoginComponent */],
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* BaseRequestOptions */]],
        })
    ], LoginModule);
    return LoginModule;
}());



/***/ })

});
//# sourceMappingURL=login.module.chunk.js.map